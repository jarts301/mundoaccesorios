function guardarEmailCliente(document,ruta){
	var email = document.getElementById("emailCliente");
	
	if(email.value!=''){
	$.ajax({
        type: "post",
        url: ruta,
        data: {Email: email.value}, // serializes the form's elements.
        success: function(data)
        {
            //data No retorna nada
        }
      });
		email.value='';
		email.placeholder='Registrado!';
	}else{
		email.placeholder='Olvidaste escribir tu email!';
	}
}

function buscar(document,direccion){
	var dato=document.getElementById('inputBusqueda');
	
	if(campoBusqueda(document,dato)){
		window.location=direccion+'&busqueda='+dato.value;
	}
}

function formatoNumero(numero, decimales, separadorDecimal, separadorMiles) {
    var partes, array;

    if ( !isFinite(numero) || isNaN(numero = parseFloat(numero)) ) {
        return "";
    }
    if (typeof separadorDecimal==="undefined") {
        separadorDecimal = ",";
    }
    if (typeof separadorMiles==="undefined") {
        separadorMiles = "";
    }

    // Redondeamos
    if ( !isNaN(parseInt(decimales)) ) {
        if (decimales >= 0) {
            numero = numero.toFixed(decimales);
        } else {
            numero = (
                Math.round(numero / Math.pow(10, Math.abs(decimales))) * Math.pow(10, Math.abs(decimales))
            ).toFixed();
        }
    } else {
        numero = numero.toString();
    }

    // Damos formato
    partes = numero.split(".", 2);
    array = partes[0].split("");
    for (var i=array.length-3; i>0 && array[i-1]!=="-"; i-=3) {
        array.splice(i, 0, separadorMiles);
    }
    numero = array.join("");

    if (partes.length>1) {
        numero += separadorDecimal + partes[1];
    }

    return numero;
}

function posicionElemento(element) {
    var top = 0, left = 0;
    var alto=element.clientHeight;
    var ancho=element.clientWidth;
    do {
        top += element.offsetTop  || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while(element);
    
    var topaux= top;
//    if(topaux>550){topaux=550;}

    return {
        top: topaux,
        left: left+ancho/2
    };
}

function miniPopUpMensaje(document,mensaje,posicion){
	document.getElementById('textoMiniMensaje').innerHTML=mensaje;
	document.getElementById('divMiniMensaje').style.display='block';
	document.getElementById('divMiniMensaje').style.top=posicion.top+'px';
	document.getElementById('divMiniMensaje').style.left=posicion.left+'px';
	var funcion=function(){
		document.getElementById('divMiniMensaje').style.display='none';
		document.getElementById('divMiniMensaje').style.opacity='1';
	}
	
	$( "#divMiniMensaje" ).fadeTo( 5000 , 0.7, funcion);
}

function miniPopUpMensaje2(document,mensaje,posicion){
	document.getElementById('textoMiniMensaje').innerHTML=mensaje;
	document.getElementById('divMiniMensaje').style.display='block';
	document.getElementById('divMiniMensaje').style.top=(posicion.top+45)+'px';
	document.getElementById('divMiniMensaje').style.left=posicion.left+'px';
	var funcion=function(){
		document.getElementById('divMiniMensaje').style.display='none';
		document.getElementById('divMiniMensaje').style.opacity='1';
	}
	
	$( "#divMiniMensaje" ).fadeTo( 5000 , 1.0, funcion);
}

function campoBusqueda(document,input){
	var resultado=false;
	var cadena = input.value;
	
	if(cadena!=null && cadena!='' && cadena.length>=3){
		resultado=true;
	}
	
	if(!resultado){
		var pos= posicionElemento(input);
		miniPopUpMensaje(document,'Escribe mínimo tres letras…',pos)
	}
	
	return resultado;
}

function mensajeCotizacion(document,input,ruta){
	var resultado=false;
	var cadena = input.value;
	
	if(cadena!=null && cadena!='' && cadena.length>=3){
		resultado=true;
	}
	
	if(!resultado){
		var pos= posicionElemento(input);
		miniPopUpMensaje2(document,'Añadido! <a href=\"'+ruta+'\">Ir a cotización</a>',pos)
	}
	
	return resultado;
}

function campoNumerico(document,input,numCaract){
	var resultado=true;
	var cadena = '',i=0;
	var valor = input.value;

	for(i=0;i<valor.length;i++){
		if(valor[i].charCodeAt()>=48 && valor[i].charCodeAt()<=57){
			cadena=cadena+valor[i];
		}else{
			resultado=false;
		}
	}
	
	if(cadena.length>numCaract){
		cadena=cadena.substring(0,numCaract);
		resultado=false;
	}
	
	input.value=cadena;
	
	if(!resultado){
		var pos= posicionElemento(input);
		//miniPopUpMensaje(document,'Solo se permiten valores numéricos.<br>Máximo '+numCaract+' caracteres.',pos)
	}
}

function campoTelefono(document,input,numCaract){
	var resultado=true;
	var cadena = '',i=0;
	var valor = input.value;

	for(i=0;i<valor.length;i++){
		if((valor[i].charCodeAt()>=48 && valor[i].charCodeAt()<=57) ||
			valor[i].charCodeAt()==45){
			cadena=cadena+valor[i];
		}else{
			resultado=false;
		}
	}
	
	if(cadena.length>numCaract){
		cadena=cadena.substring(0,numCaract);
		resultado=false;
	}
	
	input.value=cadena;
	
	if(!resultado){
		var pos= posicionElemento(input);
		miniPopUpMensaje(document,'Solo se permiten valores numéricos, y guion (-).<br>Máximo '+numCaract+' caracteres.',pos)
	}
}

function campoTexto(document,input,numCaract){
	var resultado=true;
	var cadena = input.value;
	
	if(cadena.length>numCaract){
		cadena=cadena.substring(0,numCaract);
		resultado=false;
	}
	
	input.value=cadena;
	
	if(!resultado){
		var pos= posicionElemento(input);
		//miniPopUpMensaje(document,'Maximo '+numCaract+' caracteres.',pos)
	}
}

function campoEmail(document,input,numCaract){
	var resultado=true;
//	var valor = input.value;
	var cadena=input.value;
	var emailFilter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
	
	if(!emailFilter.test(cadena)){
		resultado=false;
	}
	
	if(cadena.length>numCaract){
		cadena=cadena.substring(0,numCaract);
		resultado=false;
	}
	
	input.value=cadena;
	
	if(!resultado){
		var pos= posicionElemento(input);
		miniPopUpMensaje(document,'El correo debe tener la siguiente estructura: micorreo@correo.com<br>Máximo '+numCaract+' caracteres.',pos);
	}
}

function campoFecha(document,input,numCaract){
	var resultado=true;
	var cadena = '',i=0, contArroba=0;
	var valor = input.value;

	for(i=0;i<valor.length;i++){
		if((valor[i].charCodeAt()>=48 && valor[i].charCodeAt()<=57) ||
			valor[i].charCodeAt()==47){
			cadena=cadena+valor[i];
		}else{
			resultado=false;
		}
	}
	
	if(cadena.length>numCaract){
		cadena=cadena.substring(0,numCaract);
		resultado=false;
	}
	
	input.value=cadena;
	
	if(!resultado){
		var pos= posicionElemento(input);
		miniPopUpMensaje(document,'Solo se permiten valores numéricos, y slash (/).<br>Ejemplo:18/08/2015<br>Máximo '+numCaract+' caracteres.',pos)
	}
}

function campoPorcentaje(document,input,numCaract){
	var resultado=true;
	var cadena = '',i=0, contArroba=0;
	var valor = input.value;

	for(i=0;i<valor.length;i++){
		if((valor[i].charCodeAt()>=48 && valor[i].charCodeAt()<=57) ||
			valor[i].charCodeAt()==37){
			cadena=cadena+valor[i];
		}else{
			resultado=false;
		}
	}
	
	if(cadena.length>numCaract){
		cadena=cadena.substring(0,numCaract);
		resultado=false;
	}
	
	input.value=cadena;
	
	if(!resultado){
		var pos= posicionElemento(input);
		miniPopUpMensaje(document,'Solo se permiten valores numéricos, y porcentaje (%).<br>Máximo '+numCaract+' cifras.',pos)
	}
}

function cambiarTextoCotizacion(document,precio){
	var totalPrecio= document.getElementById('cotizacionTotal');
	var cantidadArticulos = document.getElementById('totalArticulos');
	
	var suma = parseInt(cantidadArticulos.innerHTML)+1;
	cantidadArticulos.innerHTML=suma;
	
	var dato = (totalPrecio.innerHTML.substring(1, totalPrecio.innerHTML.length)).split('.').join("");
	var sumaPrecio= parseInt(dato)+ parseInt(precio);
	totalPrecio.innerHTML='$'+formatoNumero(sumaPrecio,null,',','.');
	
}

function cotizarProducto(document,idproducto,precio,ruta){
	cambiarTextoCotizacion(document,precio);
	
	$.ajax({
        type: "POST",
        url: ruta,
        data: {idProducto: idproducto}, // serializes the form's elements.
        success: function(data)
        {
            //data No retorna nada
        }
      });
}

function eliminarProducto(document, ruta){
	$.Zebra_Dialog('<strong>Se eliminará el producto<br></strong>¿Realmente desea eliminar el producto?', {
	    	'type':     'question',
	    	'title':    'Eliminar producto',
	    	'buttons':  ['Si', 'No'],
	    	'onClose':  function(caption) {
	        if(caption=='Si'){
	        	window.location=ruta;
	        }
	    }
		});
}

function eliminarSlider(document, ruta){
	$.Zebra_Dialog('<strong>Se eliminará el slider<br></strong>¿Realmente desea eliminar el slider?', {
	    	'type':     'question',
	    	'title':    'Eliminar slider',
	    	'buttons':  ['Si', 'No'],
	    	'onClose':  function(caption) {
	        if(caption=='Si'){
	        	window.location=ruta;
	        }
	    }
		});
}

function eliminarACCA(document, ruta){
	$.Zebra_Dialog('<strong>Se eliminará la categoria de accesorios<br></strong>¿Realmente desea eliminar la categoria, se borraran tambien los productos asociados a esta?', {
	    	'type':     'question',
	    	'title':    'Eliminar categoria accesorios',
	    	'buttons':  ['Si', 'No'],
	    	'onClose':  function(caption) {
	        if(caption=='Si'){
	        	window.location=ruta;
	        }
	    }
		});
}

function eliminarMarca(document, ruta){
	$.Zebra_Dialog('<strong>Se eliminará la marca<br></strong>¿Realmente desea eliminar la marca, se borraran tambien los productos asociados a esta?', {
	    	'type':     'question',
	    	'title':    'Eliminar marca',
	    	'buttons':  ['Si', 'No'],
	    	'onClose':  function(caption) {
	        if(caption=='Si'){
	        	window.location=ruta;
	        }
	    }
		});
}




