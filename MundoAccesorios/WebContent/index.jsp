<%@page import="jarts.mundoaccesorios.general.dao.ConsultaCliente"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="jarts.mundoaccesorios.entidades.Producto"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaProducto"%>
<%@page import="jarts.mundoaccesorios.entidades.Slider"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaSlider"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";

	DecimalFormat formatoNumeros = new DecimalFormat("###,###.##");

	ConsultaCliente consultaCliente = new ConsultaCliente();
	
	ConsultaSlider consultaSlider = new ConsultaSlider();
	ArrayList<Slider> sliders = consultaSlider.consultarSliders();
	
	ConsultaProducto consultaProducto = new ConsultaProducto();
	ArrayList<Producto> celularesYTabletsAzar = consultaProducto.consultarCelularesYTabletsAzar();
	ArrayList<Producto> accesoriosAzar = consultaProducto.consultarAccesoriosAzar();
	ArrayList<Producto> portatilesYComputadoresAzar = consultaProducto.consultarPortatilesYComputadoresAzar();

%>
<!DOCTYPE html>
<html>
	<head>
		
    </head>
<body>
		
		<div class="content">
	    <!-- =================== SECTION - HERO ============================ -->
			
            <div id="hero">
            	<div id="owl-main" class="owl-carousel home-page-carousel height-lg owl-inner-nav owl-ui-lg owl-theme">
            	
            	<%if(sliders!=null){
            		for(int i=0;i< sliders.size();i++){
            			Slider sliderActual = sliders.get(i);
            	%>
            	
            	  	<div class="item" style="background-image: url(<%=basePath%>cargarImagenSliderServlet?imagen=<%=sliderActual.getImagen()%>);">
            			<div class="container">
            				<div class="caption vertical-center text-center">
            					<div class="big-text fadeInDown-1" style="opacity: 0;">
            						<%=sliderActual.getNombre()!=null?sliderActual.getNombre():""%>
            					</div>
            					<div class="fadeInDown-2 hidden-xs text" style="opacity: 0;">
            					  	<%=sliderActual.getDescripcion()!=null?sliderActual.getDescripcion():""%>
            					</div>
            					<div class="button-holder fadeInDown-3" style="opacity: 0;">
            						<%if(sliderActual.getLink()!=null){%>
            					    	<a class="btn uppercase strong btn-secondary btn-add-to-cart btn-action" href="<%=sliderActual.getLink()%>">Ver Mas</a>
            					    <%}%>
            					</div><!-- /.button-holder -->    

            				</div><!-- /.caption --> 
            			</div><!-- /.container --> 
            		</div><!-- /.item -->
            	
            	<%}}else{ %>
            		<div class="item" style="background-image: url(<%=basePath%>assets/images/sliders/slider1.jpg);">
            			<div class="container">
            				<div class="caption vertical-center text-center">
            					<div class="big-text fadeInDown-1" style="opacity: 0;">
            						Tenemos el Smartphone para ti
            					</div>
            					<div class="fadeInDown-2 hidden-xs text" style="opacity: 0;">
            					  
            					</div>
            					<div class="button-holder fadeInDown-3" style="opacity: 0;">
<%--             					    <a class="btn uppercase strong btn-secondary btn-add-to-cart btn-action" href="<%=basePath%>detail.jsp">Add To Cart</a> --%>
            					</div><!-- /.button-holder -->    

            				</div><!-- /.caption --> 
            			</div><!-- /.container --> 
            		</div><!-- /.item -->


            		<div class="item" style="background-image: url(<%=basePath%>assets/images/sliders/slider2.jpg);">
            			<div class="container">
            				<div class="caption vertical-center text-center">
            					<div class="big-text fadeInDown-1" style="opacity: 0;">
            						Las marcas más reconocidas 
            					</div>
            					<div class="fadeInDown-2 hidden-xs text" style="opacity: 0;">
            					
            					</div>
            					<div class="button-holder fadeInDown-3" style="opacity: 0;">
<%--             					    <a class="btn uppercase strong btn-secondary btn-add-to-cart btn-action" href="<%=basePath%>detail.jsp">Add To Cart</a> --%>
            					</div><!-- /.button-holder -->      
            				</div><!-- /.caption -->   
            			</div><!-- /.container --> 
            		</div><!-- /.item -->
            		
            		<div class="item" style="background-image: url(<%=basePath%>assets/images/sliders/slider3.jpg);">
            			<div class="container">
            				<div class="caption vertical-center text-center">
            					<div class="big-text fadeInDown-1" style="opacity: 0;">
            						Todo tipo de accesorios para tu móvil
            					</div>
            					<div class="fadeInDown-2 hidden-xs text" style="opacity: 0;">
            					
            					</div>
            					<div class="button-holder fadeInDown-3" style="opacity: 0;">
<%--             					    <a class="btn uppercase strong btn-secondary btn-add-to-cart btn-action" href="<%=basePath%>detail.jsp">Add To Cart</a> --%>
            					</div><!-- /.button-holder -->      
            				</div><!-- /.caption -->   
            			</div><!-- /.container --> 
            		</div><!-- /.item -->
            		
            	<%}%>

            	</div><!-- /.owl-carousel -->
            </div><!-- /#hero -->
			
        <!-- ===================== SECTION � HERO : END ====================== -->
    	    <div class="container">
<!--         		<div class="page-section block-reinsurance wow fadeInUp"> -->
<!--         			<div class="row"> -->
<!--                     	<div class="col-md-3 col-sm-3 col-xs-6 reinsurance-item"> -->
<!--                     		<div class="text-center reinsurance-image-container"> -->
<!--                     			<span class="reinsurance-icon"> -->
<!--                     				<span data-icon="&#xe021;"></span> -->
<!--                     			</span> -->
<!--                     		</div>/.text-center -->
<!--                     		<p class="reinsurance-text"> -->
<!--                     			<span class="first">Free</span> <span class="second">Shipping</span> -->
<!--                     		</p>/.reinsurance-text -->
<!--                     	</div>/.col -->

<!--                     	<div class="col-md-3 col-sm-3 col-xs-6 reinsurance-item"> -->
<!--                     		<div class="text-center reinsurance-image-container"> -->
<!--                     			<span class="reinsurance-icon"> -->
<!--                     				<span data-icon="&#xe035;"></span> -->
<!--                     			</span> -->
<!--                     		</div>/.text-center -->
<!--                     		<p class="reinsurance-text"> -->
<!--                     			<span class="first">Easy</span> <span class="second">Customize</span> -->
<!--                     		</p>/.reinsurance-text -->
<!--                     	</div>/.col -->

<!--                     	<div class="col-md-3 col-sm-3 col-xs-6 reinsurance-item"> -->
<!--                     		<div class="text-center reinsurance-image-container"> -->
<!--                     			<span class="reinsurance-icon"> -->
<!--                     				<span data-icon="&#xe105;"></span> -->
<!--                     			</span> -->
<!--                     		</div>/.text-center -->
<!--                     		<p class="reinsurance-text"> -->
<!--                     			<span class="first">Easy</span> <span class="second">Takecare</span> -->
<!--                     		</p>/.reinsurance-text -->
<!--                     	</div>/.col -->

<!--                     	<div class="col-md-3 col-sm-3 col-xs-6 reinsurance-item"> -->
<!--                     		<div class="text-center reinsurance-image-container"> -->
<!--                     			<span class="reinsurance-icon"> -->
<!--                     				<span data-icon="&#xe028;"></span> -->
<!--                     			</span> -->
<!--                     		</div>/.text-center -->
<!--                     		<p class="reinsurance-text"> -->
<!--                     			<span class="first">Lifetime</span> <span class="second">Support</span> -->
<!--                     		</p>/.reinsurance-text -->
<!--                     	</div>/.col -->
        	
<!--                     </div>/.row		 -->
<!--                 </div>/.page-section -->

        		<div class="page-section wow fadeInUp">
        			<div class="row">
                    	<div class="col-md-3">
                    		<div class="section-info">
                    			<h3 class="section-title text-md-center">Celulares y Tablets</h3>
                    			<p class="section-subtitle text-md-center">Consulta los celulares y tablets, de las mejores marcas.</p>
                    			<div class="controls clearfix hidden-xs hidden-sm">
                    				<a href="#" data-target="#recent-items-carousel" class="btn btn-primary pull-left owl-prev"><span data-icon="&#x23;"></span></a>
                    				<a href="#" data-target="#recent-items-carousel" class="btn btn-primary pull-right owl-next"><span data-icon="&#x24;"></span></a>
                    			</div><!--/.controls-->
                    		</div><!--/.section-info-->
                    	</div><!--/.col-->

                    	<div class="col-md-9">
                    		<div class="row">
                    			<div class="col-xs-2 col-sm-2 hidden-md hidden-lg no-padding">
                    				<div class="controls text-right">
                    					<a href="#" data-target="#recent-items-carousel" class="btn btn-primary owl-prev"><span data-icon="&#x23;"></span></a>
                    				</div><!-- /.controls -->
                    			</div><!-- /.col -->
                    			<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                    				<div id="recent-items-carousel" class="grid-view home-owl-carousel">
                    					
                    					<%if(celularesYTabletsAzar!=null){
            								for(int i=0;i< celularesYTabletsAzar.size();i++){
            									Producto productoActual = celularesYTabletsAzar.get(i);
            							%>
                    					
                    					<div class="product-item-column">
                    						<div class="product-item-grid-view">
                    							<div class="product-item">
                    							    <div class="product-image">
                    							        <a href="<%=basePath%>product.jsp?producto=<%=productoActual.getId()%>">
                    							            <img src="<%=basePath%>assets/images/blank.gif" 
                    							            data-echo="<%=basePath%>cargarImagenServlet?imagen=<%=productoActual.getImagen()%>" 
                    							            width="262" alt="Product" class="center-block img-responsive">
                    							        </a>
                    							    </div><!-- /.product-image -->
                    							    <div class="product-detail text-md-center">
                    							        <h3 class="product-name"><a href="<%=basePath%>product.jsp?producto=<%=productoActual.getId()%>"><%=productoActual.getNombre()%></a></h3>
                    							        <p class="product-price"><span class="price">$ <%=formatoNumeros.format(productoActual.getPrecio())%></span></p>
                    							    </div><!-- /.product-detail -->
                    							</div><!-- /.product-item -->
                    						</div><!-- /.product-item-grid-view -->
                    					</div><!-- /.product-item-column -->
                    					
                    					<%}}%>
                    				
                    				</div><!-- /#recent-items-carousel -->
                    			</div><!-- /.col -->
                    			
                    			<div class="col-xs-2 col-sm-2 hidden-md hidden-lg no-padding">
                    				<div class="controls text-left">
                    					<a href="#" data-target="#recent-items-carousel" class="btn btn-primary owl-next"><span data-icon="&#x24;"></span></a>
                    				</div><!-- /.controls -->
                    			</div><!-- /.col -->
                    		</div><!-- /.row -->
                    	</div><!-- /.col -->
                    </div><!-- /.row -->		
                </div><!-- /.page-section -->

        		<div class="page-section wow fadeInUp">
        			<div class="row">
                    	<div class="col-md-3">
                    		<div class="section-info">
                    			<h3 class="section-title text-md-center">Accesorios</h3>
                    			<p class="section-subtitle text-md-center">Conoce mas acerca de nuestros accesorios.</p>
                    			<div class="controls clearfix hidden-xs hidden-sm">
                    				<a href="#" data-target="#popular-items-carousel" class="btn btn-primary pull-left owl-prev"><span data-icon="&#x23;"></span></a>
                    				<a href="#" data-target="#popular-items-carousel" class="btn btn-primary pull-right owl-next"><span data-icon="&#x24;"></span></a>
                    			</div><!-- /.controls -->
                    		</div><!-- /.section-info -->
                    	</div><!-- /.col-md-3 -->

                    	<div class="col-md-9">
                    		<div class="row">
                    			<div class="col-xs-2 col-sm-2 hidden-md hidden-lg no-padding">
                    				<div class="controls text-right">
                    					<a href="#" data-target="#popular-items-carousel" class="btn btn-primary owl-prev"><span data-icon="&#x23;"></span></a>
                    				</div>
                    			</div><!-- /.col -->
                    			<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                    				<div id="popular-items-carousel" class="grid-view home-owl-carousel">

                    					<%if(accesoriosAzar!=null){
            								for(int i=0;i< accesoriosAzar.size();i++){
            									Producto productoActual = accesoriosAzar.get(i);
            							%>
                    					
                    					<div class="product-item-column">
                    						<div class="product-item-grid-view">
                    							<div class="product-item">
                    							    <div class="product-image">
                    							        <a href="<%=basePath%>product.jsp?producto=<%=productoActual.getId()%>">
                    							            <img src="<%=basePath%>assets/images/blank.gif" 
                    							            data-echo="<%=basePath%>cargarImagenServlet?imagen=<%=productoActual.getImagen()%>" 
                    							            width="262" alt="Product" class="center-block img-responsive">
                    							        </a>
                    							    </div><!-- /.product-image -->
                    							    <div class="product-detail text-md-center">
                    							        <h3 class="product-name"><a href="<%=basePath%>product.jsp?producto=<%=productoActual.getId()%>"><%=productoActual.getNombre()%></a></h3>
                    							        <p class="product-price"><span class="price">$ <%=formatoNumeros.format(productoActual.getPrecio())%></span></p>
                    							    </div><!-- /.product-detail -->
                    							</div><!-- /.product-item -->
                    						</div><!-- /.product-item-grid-view -->
                    					</div><!-- /.product-item-column -->
                    					
                    					<%}}%>

                    				</div><!-- /#popular-items-carousel -->
                    			</div><!-- /.col -->
                    			<div class="col-xs-2 col-sm-2 hidden-md hidden-lg no-padding">
                    				<div class="controls text-left">
                    					<a href="#" data-target="#popular-items-carousel" class="btn btn-primary owl-next"><span data-icon="&#x24;"></span></a>
                    				</div><!-- /.controls -->
                    			</div><!-- /.col -->
                    		</div><!-- /.row -->	
                    	</div><!-- /.col-md-9 -->
                    </div><!-- /.row -->		
                </div><!-- /.page-section -->
                
                <div class="page-section wow fadeInUp">
        			<div class="row">
                    	<div class="col-md-3">
                    		<div class="section-info">
                    			<h3 class="section-title text-md-center">Portatiles y Computadores</h3>
                    			<p class="section-subtitle text-md-center">Tenemos los mejores portatiles y computadores del mercado.</p>
                    			<div class="controls clearfix hidden-xs hidden-sm">
                    				<a href="#" data-target="#recent-items-carousel" class="btn btn-primary pull-left owl-prev"><span data-icon="&#x23;"></span></a>
                    				<a href="#" data-target="#recent-items-carousel" class="btn btn-primary pull-right owl-next"><span data-icon="&#x24;"></span></a>
                    			</div><!-- /.controls -->
                    		</div><!-- /.section-info -->
                    	</div><!-- /.col -->

                    	<div class="col-md-9">
                    		<div class="row">
                    			<div class="col-xs-2 col-sm-2 hidden-md hidden-lg no-padding">
                    				<div class="controls text-right">
                    					<a href="#" data-target="#recent-items-carousel" class="btn btn-primary owl-prev"><span data-icon="&#x23;"></span></a>
                    				</div><!-- /.controls -->
                    			</div><!-- /.col -->
                    			<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                    				<div id="recent-items-carousel" class="grid-view home-owl-carousel">
                    					
                    					<%if(portatilesYComputadoresAzar!=null){
            								for(int i=0;i< portatilesYComputadoresAzar.size();i++){
            									Producto productoActual = portatilesYComputadoresAzar.get(i);
            							%>
                    					
                    					<div class="product-item-column">
                    						<div class="product-item-grid-view">
                    							<div class="product-item">
                    							    <div class="product-image">
                    							        <a href="<%=basePath%>product.jsp?producto=<%=productoActual.getId()%>">
                    							            <img src="<%=basePath%>assets/images/blank.gif" 
                    							            data-echo="<%=basePath%>cargarImagenServlet?imagen=<%=productoActual.getImagen()%>" 
                    							            width="262" alt="Product" class="center-block img-responsive">
                    							        </a>
                    							    </div><!-- /.product-image -->
                    							    <div class="product-detail text-md-center">
                    							        <h3 class="product-name"><a href="<%=basePath%>product.jsp?producto=<%=productoActual.getId()%>"><%=productoActual.getNombre()%></a></h3>
                    							        <p class="product-price"><span class="price">$ <%=formatoNumeros.format(productoActual.getPrecio())%></span></p>
                    							    </div><!-- /.product-detail -->
                    							</div><!-- /.product-item -->
                    						</div><!-- /.product-item-grid-view -->
                    					</div><!-- /.product-item-column -->
                    					
                    					<%}}%>
                    				
                    				</div><!-- /#recent-items-carousel -->
                    			</div><!-- /.col -->
                    			
                    			<div class="col-xs-2 col-sm-2 hidden-md hidden-lg no-padding">
                    				<div class="controls text-left">
                    					<a href="#" data-target="#recent-items-carousel" class="btn btn-primary owl-next"><span data-icon="&#x24;"></span></a>
                    				</div><!-- /.controls -->
                    			</div><!-- /.col -->
                    		</div><!-- /.row -->
                    	</div><!-- /.col -->
                    </div><!-- /.row -->		
                </div><!-- /.page-section -->

        		<div class="row">
        		
        			<div class="col-md-4 wow fadeInUp">
        			    <div class="page-section testimonials-section">
        			    
        			    <div class="fb-page" data-href="https://www.facebook.com/mundoaccesoriosstore" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
        				<div class="fb-xfbml-parse-ignore">
        				<blockquote cite="https://www.facebook.com/mundoaccesoriosstore">
        				<a href="https://www.facebook.com/mundoaccesoriosstore">Mundo Accesorios</a></blockquote></div></div>
        			    
        			    
        			    
<!--                         	<h3 class="section-title text-md-center">Testimonials</h3> -->
<!--                             <ul class="list-unstyled list-testimonials"> -->
<!--                         		<li> -->
<!--                         			<blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</blockquote> -->
<!--                         			<p class="quoter"><span class="name">Kidesigner</span> &ndash; <span class="designation">Web Designer</span></p> -->
<!--                         		</li> -->
<!--                         		<li> -->
<!--                         			<blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</blockquote> -->
<!--                         			<p class="quoter"><span class="name">Kidesigner</span> &ndash; <span class="designation">Web Designer</span></p> -->
<!--                         		</li> -->
<!--                         		<li> -->
<!--                         			<blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</blockquote> -->
<!--                         			<p class="quoter"><span class="name">Kidesigner</span> &ndash; <span class="designation">Web Designer</span></p> -->
<!--                         		</li> -->
<!--                             </ul>/.list-testimonials -->
                        </div>		
                    </div>

        			<div class="col-md-8 wow fadeInUp">
        			    <div class="page-section videos-section">
                        	<div class="section-header">
<!--                         		<a href="#" class="pull-right hidden-xs hidden-sm">More Videos</a> -->
                        		<h3 class="section-title text-md-center">Producto destacado</h3>
                        	</div><!-- /.section-header-->
                        	
                        	<iframe id="video-player" class="video-player full-width" style="border:0px;" width="685" height="385" src="https://www.youtube.com/embed/<%=consultaCliente.consultarVideo()%>"></iframe>
<!--                         	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> -->
                        </div><!-- /.page-section-->			
                    </div><!-- /.col -->
        		</div><!-- /.row -->
    	    </div><!-- /.container -->
        </div><!-- /.content -->

</body>
</html>