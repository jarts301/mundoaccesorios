<%@page import="java.text.DecimalFormat"%>
<%@page import="jarts.mundoaccesorios.entidades.Producto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaProducto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";

	DecimalFormat formatoNumeros = new DecimalFormat("###,###.##");
	
	Object rol = request.getSession().getAttribute("clienteRol");
	
	String idCategoria =request.getParameter("categoria");
	String idMarca =request.getParameter("marca");
	String idCategoriaAccesorios =request.getParameter("acca");
	String idPagina =request.getParameter("pagina");
	String idOrden =request.getParameter("orden");
	String terminoBusqueda =request.getParameter("busqueda");

	ConsultaProducto consultaProducto = new ConsultaProducto();
	Integer cantidadProductos=0;
	Double numeroPaginas=0.0;
	String cadenaPaginador="";
	
	ArrayList<Producto> productos = null;
	if(idCategoria!=null && idMarca!=null){
		cadenaPaginador=cadenaPaginador+"categoria="+idCategoria+"&marca="+idMarca;
		cantidadProductos=consultaProducto.consultarCantidadProductos(
				Integer.parseInt(idCategoria), 
				Integer.parseInt(idMarca));
		productos = consultaProducto.consultarProductos(
				Integer.parseInt(idCategoria), 
				Integer.parseInt(idMarca), 
				Integer.parseInt(idPagina),
				Integer.parseInt(idOrden));
	}else
	if(idCategoriaAccesorios!=null){
		cadenaPaginador=cadenaPaginador+"acca="+idCategoriaAccesorios;
		cantidadProductos=consultaProducto.consultarCantidadProductos(
				Integer.parseInt(idCategoriaAccesorios));
		productos = consultaProducto.consultarProductos(
				Integer.parseInt(idCategoriaAccesorios), 
				Integer.parseInt(idPagina),
				Integer.parseInt(idOrden));
	}else
	if(terminoBusqueda!=null){
		cadenaPaginador=cadenaPaginador+"busqueda="+terminoBusqueda;
		cantidadProductos=consultaProducto.consultarCantidadProductos(terminoBusqueda);
		productos = consultaProducto.consultarProductos(
				terminoBusqueda, 
				Integer.parseInt(idPagina),
				Integer.parseInt(idOrden));
	}else{ 
		cantidadProductos=consultaProducto.consultarCantidadProductos();
		productos=consultaProducto.consultarProductos(Integer.parseInt(idPagina),
				Integer.parseInt(idOrden));
	}
	
	numeroPaginas= ((double)cantidadProductos)/8.0;

%>

<!DOCTYPE html>
<html>
	<head>
 
	</head>
<body>

        <div class="content">
            <div class="container">
                <div class="row">
            <!-- ========================================================= CONTENT ============================================================= -->
                    <div class="col-md-9 col-md-push-3">

                        <section id="category-products">
                            <h2 class="page-title text-md-center">Productos</h2>
                                <div class="page-section wow fadeInUp">
                                    <div class="tab-content">
                                        <div class="tab-pane" id="grid">
                                            <div class="row">
                                            
                                            <%if(productos!=null){
                                            for(int i=0;i<productos.size();i++){
                                            	Producto productoActual= productos.get(i);
                                            %>
                                                <div class="product-item-column col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                                    <div class="product-item-grid-view">
                                                        <div class="product-item">
                                                            <div class="product-image">
                                                                <a href="<%=basePath%>product.jsp?producto=<%=productoActual.getId()%>">
                                                                    <img src="<%=basePath%>assets/images/blank.gif" 
                                                                    data-echo="<%=productoActual.getImagen()!=null?basePath+"cargarImagenServlet?imagen="+productoActual.getImagen():""%>" 
                                                                    width="262" height="202" alt="Product" class="center-block img-responsive">
                                                                </a>
                                                            </div><!-- /.product-image -->
                                                            <div class="product-detail text-md-center">
                                                                <h3 class="product-name"><a href="<%=basePath%>product.jsp?producto=<%=productoActual.getId()%>"><%=productoActual.getNombre()%></a></h3>
                                                                <p class="product-price"><span class="price">$ <%=formatoNumeros.format(productoActual.getPrecio())%></span></p>
                                                            </div><!-- /.product-detail -->
                                                        </div><!-- /.product-item -->
                                                    </div><!-- /.product-item-grid-view -->
                                                </div><!-- /.product-item-column -->
                                            <%}
                                            }else{%>
                                            <div class="product-item-column col-md-4 col-sm-4 col-xs-12 col-lg-4" style="font-size: 17px;">
                                            	No se encontraron productos
                                            </div>
                                            <%}%>
                                            
                                            </div><!-- /.row -->
                                        </div><!-- /.tab-pane -->


                                        <div class="tab-pane active" id="list">
                                            <div class="row">
                                                <div class="product-item-list-view">
                                                
                                                <%if(productos!=null){
                                            		for(int i=0;i<productos.size();i++){
                                            		Producto productoActual= productos.get(i);
                                            	%>
                                                   <div class="row product-item">
                                                        <div class="col-md-4 col-sm-4">
                                                            <a href="<%=basePath%>product.jsp?producto=<%=productoActual.getId()%>">
                                                                <img src="<%=basePath%>assets/images/blank.gif" 
                                                                data-echo="<%=productoActual.getImagen()!=null?basePath+"cargarImagenServlet?imagen="+productoActual.getImagen():""%>" 
                                                                width="262" height="202" alt="Product" class="center-block img-responsive">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 text-md-center">
                                                            <div class="product-detail">
                                                                <h3 class="media-heading product-name"><a href="<%=basePath%>product.jsp?producto=<%=productoActual.getId()%>"><%=productoActual.getNombre()%></a></h3>
                                                                <p class="product-price"><span class="price">$ <%=formatoNumeros.format(productoActual.getPrecio())%></span></p>
                                                                <p class="product-description"><%=productoActual.getDescripcion().length()<=120?productoActual.getDescripcion():productoActual.getDescripcion().substring(0, 120)+"..."%></p>
                                                            </div><!-- /.product-detail -->
                                                            <div class="product-actions">
                                                                <div class="btn-group">
                                                                
                                                                 <%if(rol==null){%>
                                                                    <a class="btn btn-secondary strong uppercase btn-add-to-cart" onclick="mensajeCotizacion(document,this,'<%=basePath%>cart.jsp');cotizarProducto(document,'<%=productoActual.getId()%>','<%=productoActual.getPrecio()%>','<%=basePath%>cotizarProductoServlet')">
                                                                    <img src="<%=basePath%>assets/images/icon-cart.png" alt="" class="icon-btn"/> Cotizar</a>
                                                                    <%}else{%>
                                                                    <a class="btn btn-secondary strong uppercase btn-add-to-cart" 
                                                                    onclick="window.location='<%=basePath%>admin/registroProducto.jsp?producto=<%=productoActual.getId()%>'" style="background-color: #94BF10; border-color: #94BF10;">Editar</a>
                                                                    <a class="btn btn-inverse strong uppercase btn-wishlist" onclick="window.location='<%=basePath%>destacarProductoServlet?idProducto=<%=productoActual.getId()%>'"
                                                                    style="<%=productoActual.getDestacado()?"background-color: #ffd200; border-color: #ffd200;":"background-color: #836F00; border-color: #836F00;"%>"><%=productoActual.getDestacado()?"Destacado":"No destacado"%></span></a>
                                                                    <a onclick="eliminarProducto(document,'<%=basePath%>eliminarProductoServlet?idProducto=<%=productoActual.getId()%>')" class="btn btn-inverse strong uppercase btn-wishlist">Eliminar</span></a>
                                                                    <%}%>
                                                                    <!--<a href="<%=basePath%>cart.jsp" class="btn btn-inverse strong uppercase btn-wishlist"><span data-icon="&#xe030;"></span></a>-->
                                                                </div>
                                                            </div><!-- /.product-actions -->
                                                        </div><!-- /.col -->
                                                    </div><!-- /.product-item -->
                                                <%}
                                                }else{%>
                                                <div class="product-item-column col-md-4 col-sm-4 col-xs-12 col-lg-4" style="font-size: 17px;">
                                                	No se encontraron productos
                                                </div>
                                                <%}%>
                                                
                                                </div><!-- /.product-item-grid-view -->

                                            </div><!-- /.row-->
                                        </div><!-- /.tab-pane -->

                                    </div><!-- /.tab-content -->
                                </div><!-- /.page-section -->

                                <div class="page-section clearfix">
                                    <ul class="pagination">
                                    <%if(numeroPaginas>0f){
                                    	Integer contador=0;
                                    %>
                                        <li><a href="<%=basePath%>listado.jsp?orden=<%=idOrden%>&pagina=<%=(contador+1)-1>=1?(contador+1)-1:contador+1%><%=!cadenaPaginador.equals("")?"&"+cadenaPaginador:""%>">&laquo;</a></li>
                                        	<%while(numeroPaginas>(double)contador){%>
                                        		<li <%=Integer.parseInt(idPagina)==contador+1?"class=\"active\"":""%>
                                        		><a href="<%=basePath%>listado.jsp?orden=<%=idOrden%>&pagina=<%=contador+1%><%=!cadenaPaginador.equals("")?"&"+cadenaPaginador:""%>"><%=contador+1%></a></li>
                                        	<%contador++;
                                        	}%>
                                        <li><a href="<%=basePath%>listado.jsp?orden=<%=idOrden%>&pagina=<%=(contador+1)<=numeroPaginas?(contador+1):contador%><%=!cadenaPaginador.equals("")?"&"+cadenaPaginador:""%>">&raquo;</a></li>
                                    <%}%>
                                    </ul>
                                </div>
                        </section><!-- /#category-products -->

                    </div><!-- /.col-md-9 -->
            <!-- ========================================= CONTENT : END ========================================= -->    

            <!-- ========================================= SIDEBAR ========================================= -->
                    <div class="col-md-3 col-md-pull-9">

                        <aside class="sidebar row">
                            <div class="sidebar-section col-sm-4 col-xs-4 col-md-12 col-lg-12">
                                <h4 class="sidebar-title">Vista</h4>
                                <ul class="nav nav-tabs view-switcher" role="tablist" id="myTab">
                                	<%if(rol==null){%>
                                    <li><a href="#grid" role="tab" data-toggle="tab"> <span class="glyphicon glyphicon-th"></span></a></li>
                                    <%}%>
                                    <li class="active"><a href="#list" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-th-list"></span></a></li>
                                </ul><!-- /.nav -->
                            </div><!-- /.sidebar section -->
                            <div class="sidebar-section col-sm-8 col-xs-8 col-md-12 col-lg-12">
                                <h4 class="sidebar-title text-md-center">Ordenar por</h4>
                                <div class="text-md-center">
                                    <select name="sort-by" id="sort-by" class="selectpicker sidebar-sort" 
                                    onchange="window.location='<%=basePath%>listado.jsp?orden='+this.value+'&pagina=<%=idPagina%><%=!cadenaPaginador.equals("")?"&"+cadenaPaginador:""%>'">
                                    	<option value="1" <%=Integer.parseInt(idOrden)==1?"selected":""%>>Más reciente</option>
                                        <option value="2" <%=Integer.parseInt(idOrden)==2?"selected":""%>>Nombres</option>
                                        <option value="3" <%=Integer.parseInt(idOrden)==3?"selected":""%>>Menor precio</option>
                                        <option value="4" <%=Integer.parseInt(idOrden)==4?"selected":""%>>Mayor precio</option>
                                    </select>
                                </div>
                            </div>
                            <%if(rol!=null){%>
                            <div class="sidebar-section col-sm-4 col-xs-4 col-md-12 col-lg-12">
                                <h4 class="sidebar-title">Nuevo producto</h4>
                                <input class="btn btn-primary btn-block btn-lg" type="button" 
                                onclick="window.location='<%=basePath%>admin/registroProducto.jsp'" 
                                value="Agregar producto">
                            </div><!-- /.sidebar section -->
                            <%}%>
<!--                             <div class="sidebar-section col-md-12 col-lg-12 col-sm-12 col-xs-12"> -->
<!--                                 <h4 class="sidebar-title text-mute text-md-center">Filter</h4> -->

<!--                                 <section class="category"> -->
<!--                                     <h4 class="sidebar-title text-md-center">Category</h4> -->
<!--                                     <ul class="list-checkbox list-unstyled text-md-center"> -->
<!--                                         <li><label class="checkbox"><input type="checkbox"> Adipisicing Eli</label></li> -->
<!--                                         <li><label class="checkbox"><input type="checkbox"> Labore Et Dolore</label></li> -->
<!--                                         <li><label class="checkbox"><input type="checkbox"> Dolore Magna</label></li> -->
<!--                                         <li><label class="checkbox"><input type="checkbox"> Veniam</label></li> -->
<!--                                         <li><label class="checkbox"><input type="checkbox"> Minim Veniam</label></li> -->
<!--                                     </ul>/.list-checkbox -->
<!--                                 </section>/.category -->

<!--                                 <section class="size"> -->
<!--                                     <h4 class="sidebar-title text-md-center">Size</h4> -->
<!--                                     <select name="sort-by" id="sort-by-size" class="selectpicker sidebar-sort"> -->
<!--                                         <option value="1">Puppy</option> -->
<!--                                         <option value="1">Dog</option> -->
<!--                                     </select>/.select -->
<!--                                 </section>/.size -->

<!--                                 <section class="color"> -->
<!--                                     <h4 class="sidebar-title text-md-center">Color</h4> -->
<!--                                     <ul class="list-color"> -->
<!--                                         <li><label for="color-1" class="radio color-label" data-color="#94BF10"><input name="color" id="color-1" type="radio"></label></li> -->
<!--                                         <li><label for="color-2" class="radio color-label" data-color="#f26522"><input name="color" id="color-2" type="radio"></label></li> -->
<!--                                         <li><label for="color-3" class="radio color-label" data-color="#605ca8"><input name="color" id="color-3" type="radio"></label></li> -->
<!--                                         <li><label for="color-4" class="radio color-label" data-color="#a30d65"><input name="color" id="color-4" type="radio"></label></li> -->
<!--                                         <li><label for="color-5" class="radio color-label" data-color="#00aeef"><input name="color" id="color-5" type="radio"></label></li> -->
<!--                                     </ul>/.list-color -->
<!--                                 </section>/.color  -->

<!--                                 <section class="price"> -->
<!--                                     <h4 class="sidebar-title text-md-center">Price</h4> -->
<!--                                     <input type="text" class="slider" value="" data-slider-min="0" data-slider-max="20" data-slider-step="5" data-slider-value="10" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide" > -->
<!--                                 </section>/.price -->
<!--                             </div>/.section-section                    -->
                        </aside>

                    </div><!-- /.col-md-3 -->
            <!-- ========================================= SIDEBAR : END ========================================= -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.content -->

</html>