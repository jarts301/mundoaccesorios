
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";
%>
<!DOCTYPE html>
	<html>
	<head>

    </head>
<body>

		<div class="content">
        <!-- ========================================= CONTENT ========================================= -->
            <div class="container">
                <h2 class="page-title">Administrar Productos</h2>
                <p class="page-subtitle">Inicia sesión como administrador, y gestiona tus productos.</p>
            </div><!-- /.container -->

            <div class="page-section margin-top-40 contact-us-page-section">
                <iframe id="google-maps" src="https://www.google.com/maps/embed/v1/streetview?location=10.9842%2C-74.7898&key=AIzaSyBI4fN_gAfPajqBa_xCS7RAzmypv55OdzI"></iframe>

                <div class="contact-us-form">
                    <div class="container">
                        <div class="row">
                            <div class="contact-us-form-section col-md-5 col-sm-6 col-sm-offset-7">
                                <div class="page-section no-margin-top">
                                    <h3 class="section-title">
                                        Administrador
                                    </h3>
                                    <form role="form" method="post" action="<%=basePath%>logInServlet">
                                        <div class="form-group">
                                            <label for="full-name" class="sr-only">Email</label>
                                            <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="subject" class="sr-only">Contraseña</label>
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-lg btn-block">Ingresar</button>
                                        </div><!-- /.form-group -->
                                    </form><!-- /form -->
                                </div><!-- /.page-section -->
                            </div><!-- /.contact-us-form-section -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.contact us form -->  
                
            </div><!-- /.page-section -->
        <!-- ========================================= CONTENT : END ========================================= -->
        </div><!-- /.content -->

</body>
</html>