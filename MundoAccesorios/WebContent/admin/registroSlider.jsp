<%@page import="jarts.mundoaccesorios.entidades.Slider"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaSlider"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaCategoria"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";

	Object rol = request.getSession().getAttribute("clienteRol");
	
	String idSlider = request.getParameter("slider");
	ConsultaSlider consultaSlider = new ConsultaSlider();
	Slider slider = null;
	if(idSlider!=null){
		slider = consultaSlider.consultarSlider(Integer.parseInt(idSlider));
	}

%>
<!DOCTYPE html>
	<html>
	<head>

    </head>
<body>
		<%if(rol!=null){%>
		<div class="content">
        <!-- ========================================= CONTENT ========================================= -->
            <div class="container">
                <h2 class="page-title">Registrar Slider</h2>
                <p class="page-subtitle">Registra un nuevo slider, para que aparezca al inicio de la pagina.</p>
            
            	<form 
            	action="<%=idSlider==null?basePath+"registrarSlideServlet":basePath+"actualizarSliderServlet"%>"
            	method="post" 
            	<%=idSlider==null?"enctype=\"multipart/form-data\"":""%> 
            	class="section-title" style="font-size: 18px; color: black;">

            	<input type="hidden" name="hidSlider" id="hidSlider" value="<%=idSlider!=null?idSlider:""%>">
            	
            		<label>1.Nombre </label><br>
            		<input class="form-control" type="text" name="nombre" value="<%=idSlider!=null?slider.getNombre()!=null?slider.getNombre():"":""%>" oninput="campoTexto(document,this,150)"><br>
            		<label>2.Descripción</label><br>
            		<textarea class="form-control" name="descripcion" oninput="campoTexto(document,this,500)" 
            		style="resize:none;height: 100px;"><%=idSlider!=null?slider.getDescripcion()!=null?slider.getDescripcion():"":""%></textarea><br>
            		
            		<label>3.Imagen (Se recomienda una imagen con resolucion de 1920 x 720 pixeles aproximadamente)</label><br>
            		<%if(idSlider!=null){%>
            		<img src="<%=basePath%>cargarImagenSliderServlet?imagen=<%=slider.getImagen()%>" width="100%"><br><br>
            		<%}else{%>
            		<input class="form-control" name="imagen" type="file" accept="image/jpeg" required><br>
            		<%}%>
            		
            		<label>4.Link (Boton de mas informacion sobre el producto del slider.)</label><br>
            		<input class="form-control" name="link" type="text" value="<%=idSlider!=null?slider.getLink()!=null?slider.getLink():"":""%>" oninput="campoTexto(document,this,500)"><br>
            		
            		<label>5.Posicion (Lugar en que aparecera el slider, debe ser diferente para cada slider)</label><br>
            		<input class="form-control" name="posicion" type="text" placeholder="valor numerico 1 ó 2 ó 3 etc..." 
            		value="<%=idSlider!=null?slider.getPosicion():""%>" required oninput="campoNumerico(document,this,11)"><br>
            		
            		<%if(idSlider!=null){%>
            		<input type="submit" value="Actualizar" class="btn btn-primary btn-block btn-lg"><br>
            		<%}else{%>
            		<input type="submit" value="Registrar" class="btn btn-primary btn-block btn-lg"><br>
            		<%}%>
            		
            	</form>
                
        <!-- ========================================= CONTENT : END ========================================= -->
        	</div><!-- /.container -->
        </div><!-- /.content -->
        
        <%}else{%>
        <div class="content">
        <!-- ========================================= CONTENT ========================================= -->
            <div class="container">
                <h3 class="page-title">No tiene permiso para ingresar a esta pagina <a href="<%=basePath%>index.jsp">Regresar al inicio</a> </h2>
        	</div><!-- /.container -->
        </div><!-- /.content -->
        <%}%>

</body>
</html>