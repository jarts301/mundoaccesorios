<%@page import="jarts.mundoaccesorios.entidades.Slider"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaSlider"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="jarts.mundoaccesorios.entidades.Producto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaProducto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";

	DecimalFormat formatoNumeros = new DecimalFormat("###,###.##");
	
	Object rol = request.getSession().getAttribute("clienteRol");
	
	ConsultaSlider consultaSlider = new ConsultaSlider();
	ArrayList<Slider> sliders = consultaSlider.consultarSliders();

%>

<!DOCTYPE html>
<html>
	<head>
 
	</head>
<body>

        <div class="content">
            <div class="container">
                <div class="row">
            <!-- ========================================================= CONTENT ============================================================= -->
                    <div class="col-md-9 col-md-push-3">

                        <section id="category-products">
                            <h2 class="page-title text-md-center">Sliders</h2>
                                <div class="page-section wow fadeInUp">
                                    <div class="tab-content">
                                        <div class="tab-pane" id="grid">
                                            <div class="row">
                                            
                                            <%if(sliders!=null){
                                            for(int i=0;i<sliders.size();i++){
                                            	Slider sliderActual= sliders.get(i);
                                            %>
                                                <div class="product-item-column col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                                    <div class="product-item-grid-view">
                                                        <div class="product-item">
                                                            <div class="product-image">
                                                                    <img src="<%=basePath%>assets/images/blank.gif" 
                                                                    data-echo="<%=sliderActual.getImagen()!=null?basePath+"cargarImagenSliderServlet?imagen="+sliderActual.getImagen():""%>" 
                                                                    width="262" height="202" alt="Product" class="center-block img-responsive">
                                                            </div><!-- /.product-image -->
                                                            <div class="product-detail text-md-center">
                                                                <h3 class="product-name"><%=sliderActual.getNombre()!=null?sliderActual.getNombre():""%></h3>
                                                                <p class="product-price"><span class="price">Posición:<%=sliderActual.getPosicion()%></span></p>
                                                            </div><!-- /.product-detail -->
                                                        </div><!-- /.product-item -->
                                                    </div><!-- /.product-item-grid-view -->
                                                </div><!-- /.product-item-column -->
                                            <%}
                                            }else{%>
                                            <div class="product-item-column col-md-4 col-sm-4 col-xs-12 col-lg-4" style="font-size: 17px;">
                                            	No se encontraron productos
                                            </div>
                                            <%}%>
                                            
                                            </div><!-- /.row -->
                                        </div><!-- /.tab-pane -->


                                        <div class="tab-pane active" id="list">
                                            <div class="row">
                                                <div class="product-item-list-view">
                                                
                                                <%if(sliders!=null){
                                            		for(int i=0;i<sliders.size();i++){
                                            		Slider sliderActual= sliders.get(i);
                                            	%>
                                                   <div class="row product-item">
                                                        <div class="col-md-4 col-sm-4">
                                                                <img src="<%=basePath%>assets/images/blank.gif" 
                                                                data-echo="<%=sliderActual.getImagen()!=null?basePath+"cargarImagenSliderServlet?imagen="+sliderActual.getImagen():""%>" 
                                                                width="262" height="202" alt="Product" class="center-block img-responsive">
                                                        </div>
                                                        <div class="col-md-8 col-sm-8 text-md-center">
                                                            <div class="product-detail">
                                                                <h3 class="media-heading product-name"><%=sliderActual.getNombre()!=null?sliderActual.getNombre():""%></h3>
                                                                <p class="product-price"><span class="price">Posición: <%=sliderActual.getPosicion()%></span></p>
                                                                <p class="product-description"><%=sliderActual.getDescripcion()!=null?sliderActual.getDescripcion().length()<=120?sliderActual.getDescripcion():sliderActual.getDescripcion().substring(0, 120)+"...":""%></p>
                                                            </div><!-- /.product-detail -->
                                                            <div class="product-actions">
                                                                <div class="btn-group">
                                                                <a class="btn btn-secondary strong uppercase btn-add-to-cart" 
                                                                onclick="window.location='<%=basePath%>admin/registroSlider.jsp?slider=<%=sliderActual.getId()%>'" style="background-color: #94BF10; border-color: #94BF10;">Editar</a>
                                                                <a onclick="eliminarSlider(document,'<%=basePath%>eliminarSliderServlet?idSlider=<%=sliderActual.getId()%>')" class="btn btn-inverse strong uppercase btn-wishlist">Eliminar</span></a>
                                                                <!--<a href="<%=basePath%>cart.jsp" class="btn btn-inverse strong uppercase btn-wishlist"><span data-icon="&#xe030;"></span></a>-->
                                                                </div>
                                                            </div><!-- /.product-actions -->
                                                        </div><!-- /.col -->
                                                    </div><!-- /.product-item -->
                                                <%}
                                                }else{%>
                                                <div class="product-item-column col-md-4 col-sm-4 col-xs-12 col-lg-4" style="font-size: 17px;">
                                                	No hay sliders registrados
                                                </div>
                                                <%}%>
                                                
                                                </div><!-- /.product-item-grid-view -->

                                            </div><!-- /.row-->
                                        </div><!-- /.tab-pane -->

                                    </div><!-- /.tab-content -->
                                </div><!-- /.page-section -->

                        </section><!-- /#category-products -->

                    </div><!-- /.col-md-9 -->
            <!-- ========================================= CONTENT : END ========================================= -->    

            <!-- ========================================= SIDEBAR ========================================= -->
                    <div class="col-md-3 col-md-pull-9">

                        <aside class="sidebar row">
                            <div class="sidebar-section col-sm-4 col-xs-4 col-md-12 col-lg-12">
                                <h4 class="sidebar-title">Vista</h4>
                                <ul class="nav nav-tabs view-switcher" role="tablist" id="myTab">
                                	<%if(rol==null){%>
                                    <li><a href="#grid" role="tab" data-toggle="tab"> <span class="glyphicon glyphicon-th"></span></a></li>
                                    <%}%>
                                    <li class="active"><a href="#list" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-th-list"></span></a></li>
                                </ul><!-- /.nav -->
                            </div><!-- /.sidebar section -->
                            <div class="sidebar-section col-sm-4 col-xs-4 col-md-12 col-lg-12">
                                <h4 class="sidebar-title">Nuevo Slider</h4>
                                <input class="btn btn-primary btn-block btn-lg" type="button" 
                                onclick="window.location='<%=basePath%>admin/registroSlider.jsp'" 
                                value="Agregar slider">
                            </div><!-- /.sidebar section -->
                        </aside>

                    </div><!-- /.col-md-3 -->
            <!-- ========================================= SIDEBAR : END ========================================= -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.content -->

</html>