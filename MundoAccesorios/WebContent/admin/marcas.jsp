
<%@page import="jarts.mundoaccesorios.entidades.Producto"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaProducto"%>
<%@page import="jarts.mundoaccesorios.entidades.AccesoriosCategoria"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaAccesoriosCategoria"%>
<%@page import="jarts.mundoaccesorios.entidades.Marca"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaMarca"%>
<%@page import="jarts.mundoaccesorios.entidades.Categoria"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaCategoria"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";

	Object rol = request.getSession().getAttribute("clienteRol");
	
	ConsultaMarca consultaMarca = new ConsultaMarca();
	ArrayList<Marca> marcas = consultaMarca.consultarMarcas();

%>
<!DOCTYPE html>
	<html>
	<head>

    </head>
<body>
		<%if(rol!=null){%>
		<div class="content">
        <!-- ========================================= CONTENT ========================================= -->
            <div class="container">
                <h2 class="page-title">Marcas</h2>
                <p class="page-subtitle">Consulta y registra nuevas marcas.</p>
                
                <%if(marcas!=null){
                for(int i=0; i< marcas.size(); i++){
                %>
                	<div class="form-control" style="font-size: 18px;background-color:<%=i%2==0?"#CCFFE5":"#FFFFFF"%>" >
                	<table>
                	 <tr><td style="width: 95%"><%=marcas.get(i).getNombre()%></td>
                	 <td style="font-size: 16px;"><a onclick="eliminarMarca(document,'<%=basePath%>eliminarMarcaServlet?idMarca=<%=marcas.get(i).getId()%>')">Eliminar</a></td></tr>
                	</table>
                	</div>
                <%}}%>
                <br>
                <form action="<%=basePath%>registrarMarcaServlet" method="post" class="section-title" style="font-size: 18px; color: black;">
                	<table>
                	 <tr><td style="width: 95%"><input class="form-control" name="nombre" type="text" 
                	 oninput="campoTexto(document,this,150)" placeholder="Ingresa una nueva marca (sin espacios)" required>
                	 </td><td style="font-size: 16px;"><input type="submit" value="Guardar" 
                	 style="color: white; background-color: #94BF10; border-color: #94BF10; border-style: solid;"></td></tr>
                	</table>
                </form>
        <!-- ========================================= CONTENT : END ========================================= -->
        	</div><!-- /.container -->
        </div><!-- /.content -->
        
        <%}else{%>
        <div class="content">
        <!-- ========================================= CONTENT ========================================= -->
            <div class="container">
                <h3 class="page-title">No tiene permiso para ingresar a esta pagina <a href="<%=basePath%>index.jsp">Regresar al inicio</a> </h2>
        	</div><!-- /.container -->
        </div><!-- /.content -->
        <%}%>

</body>
</html>