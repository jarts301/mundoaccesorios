
<%@page import="jarts.mundoaccesorios.entidades.Producto"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaProducto"%>
<%@page import="jarts.mundoaccesorios.entidades.AccesoriosCategoria"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaAccesoriosCategoria"%>
<%@page import="jarts.mundoaccesorios.entidades.Marca"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaMarca"%>
<%@page import="jarts.mundoaccesorios.entidades.Categoria"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaCategoria"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";

	Object rol = request.getSession().getAttribute("clienteRol");
	
	String idProducto = request.getParameter("producto");
	ConsultaProducto consultaProducto = new ConsultaProducto();
	Producto producto = null;
	if(idProducto!=null){
		producto = consultaProducto.consultarProducto(Integer.parseInt(idProducto));
	}

	ConsultaCategoria consultaCategoria = new ConsultaCategoria();
	ArrayList<Categoria> categorias = consultaCategoria.consultarCategorias();
	
	ConsultaMarca consultaMarca = new ConsultaMarca();
	ArrayList<Marca> marcas = consultaMarca.consultarMarcas();
	
	ConsultaAccesoriosCategoria consultaAccesoriosCategoria = new ConsultaAccesoriosCategoria();
	ArrayList<AccesoriosCategoria> accesoriosCategorias = consultaAccesoriosCategoria.consultarAccesoriosCategorias();

%>
<!DOCTYPE html>
	<html>
	<head>

    </head>
<body>
		<%if(rol!=null){%>
		<div class="content">
        <!-- ========================================= CONTENT ========================================= -->
            <div class="container">
                <h2 class="page-title">Registro de producto</h2>
                <p class="page-subtitle">Registra un nuevo producto, para que lo vean tus clientes</p>
            
            	<form 
            	action="<%=idProducto==null?basePath+"registrarProductoServlet":basePath+"actualizarProductoServlet"%>"
            	method="post" 
            	<%=idProducto==null?"enctype=\"multipart/form-data\"":""%> 
            	class="section-title" style="font-size: 18px; color: black;">

            	<input type="hidden" name="hidProducto" id="hidProducto" value="<%=idProducto!=null?idProducto:""%>">
            	
            		<label>1.Nombre (No puede haber dos productos con el mismo nombre)</label><br>
            		<input class="form-control" type="text" name="nombre" value="<%=idProducto!=null?producto.getNombre():""%>" oninput="campoTexto(document,this,150)" required <%=idProducto!=null?"disabled":""%>><br>
            		<label>2.Descripción</label><br>
            		<textarea class="form-control" name="descripcion" oninput="campoTexto(document,this,4000)" style="resize:none;height: 100px;"><%=idProducto!=null?producto.getDescripcion()!=null?producto.getDescripcion():"":""%></textarea><br>
            		<label>3.Precio</label><br>
            		<input class="form-control" name="precio" type="text" value="<%=idProducto!=null?producto.getPrecio():""%>" oninput="campoNumerico(document,this,11)" required><br>
            		
            		<label>4.Imagen</label><br>
            		<%if(idProducto!=null){%>
            		<img src="<%=basePath%>cargarImagenServlet?imagen=<%=producto.getImagen()%>" width="300px"><br><br>
            		<%}else{%>
            		<input class="form-control" name="imagen" type="file" accept="image/jpeg" required><br>
            		<%}%>
            		
            		<label>5.Link (Link de la pagina oficial o mas detallada del producto)</label><br>
            		<input class="form-control" name="link" type="text" value="<%=idProducto!=null?producto.getLink()!=null?producto.getLink():"":""%>" oninput="campoTexto(document,this,500)"><br>
            		<label>6.Categoria</label><br>
            		<select class="form-control" name="categoria" required>
            		<%if(categorias!=null){
            		for(int i=0;i< categorias.size(); i++){
            		%>
            			<option value="<%=categorias.get(i).getId()%>" <%=idProducto!=null?producto.getIdCategoria()==categorias.get(i).getId()?"selected":"":""%>><%=categorias.get(i).getNombre()%></option>
            		<%}}%>
            		</select><br>
            		<label>7.Marca</label><br>
            		<select class="form-control" name="marca" required>
            		<%if(marcas!=null){
            		for(int i=0;i< marcas.size(); i++){
            		%>
            			<option value="<%=marcas.get(i).getId()%>" <%=idProducto!=null?producto.getIdMarca()==marcas.get(i).getId()?"selected":"":""%>><%=marcas.get(i).getNombre()%></option>
            		<%}}%>
            		</select><br>
            		<label>8.Categoria de accesorios (Aplica si es un accesorio. no celulares, portatiles, computadores o tablets)</label><br>
            		<select class="form-control" name="acca" required>
            			<option value="0">No aplica</option>
            		<%if(accesoriosCategorias!=null){
            		for(int i=0;i< accesoriosCategorias.size(); i++){
            		%>
            			<option value="<%=accesoriosCategorias.get(i).getId()%>" <%=idProducto!=null?producto.getIdAccesoriosCategoria()==accesoriosCategorias.get(i).getId()?"selected":"":""%>><%=accesoriosCategorias.get(i).getNombre()%></option>
            		<%}}%>
            		</select><br><br>
            		<%if(idProducto!=null){%>
            		<input type="submit" value="Actualizar" class="btn btn-primary btn-block btn-lg"><br>
            		<%}else{%>
            		<input type="submit" value="Registrar" class="btn btn-primary btn-block btn-lg"><br>
            		<%}%>
            		
            	</form>
                
        <!-- ========================================= CONTENT : END ========================================= -->
        	</div><!-- /.container -->
        </div><!-- /.content -->
        
        <%}else{%>
        <div class="content">
        <!-- ========================================= CONTENT ========================================= -->
            <div class="container">
                <h3 class="page-title">No tiene permiso para ingresar a esta pagina <a href="<%=basePath%>index.jsp">Regresar al inicio</a> </h2>
        	</div><!-- /.container -->
        </div><!-- /.content -->
        <%}%>

</body>
</html>