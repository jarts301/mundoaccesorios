
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaCliente"%>
<%@page import="jarts.mundoaccesorios.admin.dao.ConsultaCorreos"%>
<%@page import="jarts.mundoaccesorios.entidades.Producto"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaProducto"%>
<%@page import="jarts.mundoaccesorios.entidades.AccesoriosCategoria"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaAccesoriosCategoria"%>
<%@page import="jarts.mundoaccesorios.entidades.Marca"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaMarca"%>
<%@page import="jarts.mundoaccesorios.entidades.Categoria"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaCategoria"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";

	Object rol = request.getSession().getAttribute("clienteRol");
	
	ConsultaCliente consultaCliente = new ConsultaCliente();
	String video= consultaCliente.consultarVideo();

%>
<!DOCTYPE html>
	<html>
	<head>

    </head>
<body>
		<%if(rol!=null){%>
		<div class="content">
        <!-- ========================================= CONTENT ========================================= -->
            <div class="container">
                <h2 class="page-title">Video de inicio</h2>
                <p class="page-subtitle">Aqui puedes ver y cambiar el video de youtube que aparece en el inicio de la pagina.</p>
                
                <form action="<%=basePath%>actualizarVideoServlet" method="post" class="section-title" style="font-size: 18px; color: black;">
                	<table>
                	 <tr><td style="font-weight: normal;">Ingresa el codigo (resaltado) del video. Ejemplo: https://www.youtube.com/watch?v=<strong style="color: red;">b19e0_NlD-U</strong>)<br></td></tr>
                	 <tr><td style="width: 95%"><input name="codigo" type="text" 
                	 oninput="campoTexto(document,this,150)" class="form-control" value="<%=video%>" required>
                	 </td><td style="font-size: 16px;"><input type="submit" value="Guardar" 
                	 style="color: white; background-color: #94BF10; border-color: #94BF10; border-style: solid;"></td></tr>
                	</table>
                </form>
                
                <iframe id="video-player" class="video-player full-width" style="border:0px;" width="685" height="385" src="https://www.youtube.com/embed/<%=video%>"></iframe>

        <!-- ========================================= CONTENT : END ========================================= -->
        	</div><!-- /.container -->
        </div><!-- /.content -->
        
        <%}else{%>
        <div class="content">
        <!-- ========================================= CONTENT ========================================= -->
            <div class="container">
                <h3 class="page-title">No tiene permiso para ingresar a esta pagina <a href="<%=basePath%>index.jsp">Regresar al inicio</a> </h2>
        	</div><!-- /.container -->
        </div><!-- /.content -->
        <%}%>

</body>
</html>