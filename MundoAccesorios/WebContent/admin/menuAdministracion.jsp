
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";

	Object rol = request.getSession().getAttribute("clienteRol");

%>
<!DOCTYPE html>
	<html>
	<head>

    </head>
<body>
		<%if(rol!=null){%>
		<div class="content">
        <!-- ========================================= CONTENT ========================================= -->
            <div class="container">
                <h2 class="page-title">Menu administración</h2>
                <p class="page-subtitle">Gestiona tu pagina desde esta seccion</p>
            
            	<input type="button" class="btn btn-primary btn-block btn-lg" onclick="window.location='<%=basePath%>listado.jsp?orden=1&pagina=1'" value="Productos" style="width: 85%; height: 40px; font-size: 18px;"><br>
            	<input type="button" class="btn btn-primary btn-block btn-lg" onclick="window.location='<%=basePath%>admin/categoriasAccesorios.jsp'" value="Categorias accesorios" style="width: 85%; height: 40px; font-size: 18px;"><br>
            	<input type="button" class="btn btn-primary btn-block btn-lg" onclick="window.location='<%=basePath%>admin/marcas.jsp'" value="Marcas" style="width: 85%; height: 40px; font-size: 18px;"><br>
            	<input type="button" class="btn btn-primary btn-block btn-lg" onclick="window.location='<%=basePath%>admin/correosUsuarios.jsp'" value="Correos electronicos registrados" style="width: 85%; height: 40px; font-size: 18px;"><br>
            	<input type="button" class="btn btn-primary btn-block btn-lg" onclick="window.location='<%=basePath%>admin/listadoSliders.jsp'" value="Sliders pagina principal" style="width: 85%; height: 40px; font-size: 18px;"><br>
            	<input type="button" class="btn btn-primary btn-block btn-lg" onclick="window.location='<%=basePath%>admin/video.jsp'" value="Video" style="width: 85%; height: 40px; font-size: 18px;">
                
        <!-- ========================================= CONTENT : END ========================================= -->
        	</div><!-- /.container -->
        </div><!-- /.content -->
        <%}else{%>
        <div class="content">
        <!-- ========================================= CONTENT ========================================= -->
            <div class="container">
                <h3 class="page-title">No tiene permiso para ingresar a esta pagina <a href="<%=basePath%>index.jsp">Regresar al inicio</a> </h2>
        	</div><!-- /.container -->
        </div><!-- /.content -->
        <%}%>

</body>
</html>