<?xml version="1.0" encoding="UTF-8" ?>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@page import="jarts.mundoaccesorios.entidades.AccesoriosCategoria"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaAccesoriosCategoria"%>
<%@page import="jarts.mundoaccesorios.entidades.Marca"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaMarca"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaProducto"%>
<%@page import="jarts.mundoaccesorios.entidades.Producto"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";

	DecimalFormat formatoNumeros = new DecimalFormat("###,###.##");
	
	Object rol = request.getSession().getAttribute("clienteRol");
	
	ConsultaProducto consultaProducto= new ConsultaProducto();
	String cotizacion = (String)request.getSession().getAttribute("cotizacion");
	Integer cantidadProductosCot=0;
	HashMap<Integer, Integer> hmProductos=null;
	ArrayList<Producto> productosCotizacion=null;
	
	if(cotizacion!=null){
		cantidadProductosCot=cotizacion.substring(0,cotizacion.length()-1).split(",").length;
		hmProductos = consultaProducto.hacerMapCotizacion(
				cotizacion.substring(0,cotizacion.length()-1).split(","));
		productosCotizacion = consultaProducto.consultarProductos(hmProductos);
	}
	
	ArrayList<Producto> productosDestacados = consultaProducto.consultarProductosDestacados();
	
	ConsultaMarca consultaMarca = new ConsultaMarca();
	ArrayList<Marca> marcasConCelulares = consultaMarca.consultarMarcasConCelulares();
	ArrayList<Marca> marcasConTablets = consultaMarca.consultarMarcasConTablets();
	ArrayList<Marca> marcasConPortatiles = consultaMarca.consultarMarcasConPortatiles();
	ArrayList<Marca> marcasConComputadores = consultaMarca.consultarMarcasConComputadores();
	
	ConsultaAccesoriosCategoria consultaAccesoriosCategoria = new ConsultaAccesoriosCategoria();
	ArrayList<AccesoriosCategoria> acesoriosCategorias = consultaAccesoriosCategoria.consultarAccesoriosCategorias();
	
%>
<!DOCTYPE html>
<html>
<head>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">

	<%-- <title><decorator:title /></title> --%>
	<title>Mundo Accesorios</title>
	<link rel="icon" href="<%=basePath%>favicon.ico" type="image/x-icon" />
	
	<!-- Bootstrap Core CSS -->
	<link rel="stylesheet" href="<%=basePath%>assets/css/bootstrap.min.css">
	
	<!-- 	Scripts -->
	<script src="<%=basePath%>assets/js/jquery-1.11.0.min.js"></script>
	<script src="<%=basePath%>assets/js/zebra_dialog.js"></script>
	<script src="<%=basePath%>assets/js/general.js"></script>

	<!-- Customizable CSS -->
	<link rel="stylesheet" href="<%=basePath%>assets/css/main.css">
	<link rel="stylesheet" href="<%=basePath%>assets/css/green.css">
	<link rel="stylesheet" href="<%=basePath%>assets/css/bootstrap-select.min.css">
	<link rel="stylesheet" href="<%=basePath%>assets/css/owl.carousel.css">
	<link rel="stylesheet" href="<%=basePath%>assets/css/owl.transitions.css">
	<link rel="stylesheet" href="<%=basePath%>assets/css/animate.min.css">
	<link rel="stylesheet" href="<%=basePath%>assets/css/flat/zebra_dialog.css">
	
	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Asap:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

	<!-- Icons/Glyphs -->
	<link rel="stylesheet" href="<%=basePath%>assets/css/elegant-fonts.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<%=basePath%>assets/images/favicon.ico">
	
	<decorator:head />
	
</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	<div class="wrapper">
	
	<!-- ======================= HEADER =================================== -->
        <header class="header" style="background-color: #94BF10;">
	        <div class="container">
		        <div class="row">
			        <div class="col-md-4 col-sm-4 col-xs-12">
		<!--=================================LOGO=============================== -->
           <div class="logo text-md-center">
                <h1>
                    <a href="<%=basePath%>index.jsp">
                        <img src="<%=basePath%>assets/images/logo.jpg" width="180"/>
                    </a>
                </h1>
            </div><!-- /.logo -->
        <!-- ============================= LOGO : END ========================== -->			    </div><!-- /.col -->

			        <div class="col-md-3 col-sm-3 col-xs-12">
		<!-- ========================== SEARCH BAR=============================== -->
                        <div role="form" class="block-search" style="margin-top:15px">
                        	<div class="input-group">
                        		<input type="text" class="form-control" id="inputBusqueda" name="inputBusqueda" placeholder="Buscar productos..">
                        		<span class="input-group-btn">
                        			<button class="btn btn-primary-very-light search-button" type="button" onclick="buscar(document,'<%=basePath%>listado.jsp?orden=1&pagina=1');">
                        			<span class="glyphicon glyphicon-search" ></span></button>
                        		</span>
                        	</div><!-- /input-group -->
                        </div><!-- /block-search -->
                         <form role="form" class="block-search">
                        	<div class="input-group">
                        		<span class="input-group-btn">
                        			<textarea class="form-control"
                        			style="width: 350px;height: 120px; resize: none; cursor: text; font-size: 16px; font-weight: bold;
                        			overflow: hidden; background-color: #94BF10; color:#fff;margin-top:-10px;" 
                        			readonly><%="Teléfono:\n\t(+57) (5) 3177864\nDirección:\n\tC.C Parque Central Local 190 y 198 'A'\n\tBarranquilla, Colombia"%></textarea>
                        		</span>
                        	</div>
                        </form>
        <!-- ========================== SEARCH BAR :END ===========================-->			
                    </div><!-- /.col -->

			        <div class="col-md-5 col-sm-5 col-xs-12">
		<!-- ======================== BLOCK HEADER LINKS========================= -->
                        <div class="block-header-links" >
                        	<ul class="nav nav-header-link pull-right">
<!--                         		<li><a href="#"  style="color: #FFFFFF;">Iniciar sesión</a></li> -->
<!--                         		<li><a href="#"  style="color: #FFFFFF;">Wishlist</a></li> -->
                        		<li><a href="<%=basePath%>cart.jsp"  
                        		style="color: #FFFFFF;">Total Cotización: <span id="cotizacionTotal" 
                        		class="total-cart-amount"  style="color: #FFFFFF;"
                        		>$<%
                        		Integer suma = 0;
                        		if(hmProductos!=null ){
                        			Iterator<Integer> keySetIterator = hmProductos.keySet().iterator(); 
                        			while(keySetIterator.hasNext()){ 
                        				Integer key = keySetIterator.next();
                        				suma = suma + (hmProductos.get(key)*consultaProducto.obtenerProducto(productosCotizacion, key).getPrecio());
                        			}
                        		}
                        		out.print(formatoNumeros.format(suma));
                        		%></span></a></li>
                        	</ul><!-- /.nav-header-link -->
                        </div><!-- /.block-header-links -->	
        <!-- =================== BLOCK HEADER LINKS :END ======================== -->  </div><!-- /.col -->		
		        </div><!-- /.row -->
		        <a class="btn-shopping-cart hidden-xs" data-toggle="modal"><img src="<%=basePath%>assets/images/icon-cart.png" 
		        class="icon-cart" width="20" height="18" alt="Cart Icon" />
		        <span id="totalArticulos" class="cart-items-count"><%=cantidadProductosCot%></span></a>
	        </div><!-- /.container -->

	    <!-- =================== NAVBAR PRIMARY ============================== -->
            <nav class="yamm navbar navbar-primary animate-dropdown" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button id="btn-navbar-primary-collapse" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div><!-- /.navbar-header -->
                    <div class="collapse navbar-collapse" id="navbar-primary-collapse">
                        <ul class="nav navbar-nav">
                        
                        <%if(rol==null){%>
                        
                            <li><a href="<%=basePath%>index.jsp">Inicio</a></li>
                            
                            <li class="dropdown yamm-fw">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Más vendido</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="yamm-content grid-view">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span class="megamenu-title">Artículos más vendidos</span>
                                                </div>             
                                                <%if(productosDestacados!=null){
                                                for(int i=0; i<productosDestacados.size(); i++){
                                                	Producto productoActual = productosDestacados.get(i);
                                                %>                     
                                                <div class="product-item-column col-md-2 col-sm-4 col-xs-12 col-lg-2">
                                                    <div class="product-item-grid-view">
                                                        <div class="product-item">
                                                            <div class="product-image">
                                                                <a href="<%=basePath%>product.jsp?producto=<%=productoActual.getId()%>">
                                                                    <img src="<%=basePath%>assets/images/blank.gif" 
                                                                    data-echo="<%=productoActual.getImagen()!=null?basePath+"cargarImagenServlet?imagen="+productoActual.getImagen():""%>" 
                                                                    width="262" alt="Product" class="center-block img-responsive">                
                                                                </a>
                                                            </div><!-- /.product-image -->
                                                            <div class="product-detail text-md-center">
                                                                <h3 class="product-name"><a href="<%=basePath%>product.jsp?producto=<%=productoActual.getId()%>"><%=productoActual.getNombre()%></a></h3>
                                                                <p class="product-price"><span class="price">$ <%=formatoNumeros.format(productoActual.getPrecio())%></span></p>
                                                            </div><!-- /.product-detail -->
                                                        </div><!-- /.product-item -->
                                                    </div><!-- /.product-item-grid-view -->
                                                </div><!-- /.product-item-column -->
                                                <%}
                                                }%>
                                               
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Accesorios</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                                            
                                            <%if(acesoriosCategorias!=null){
                                            	Integer contador =1;
                                      			for(int i=0; i<acesoriosCategorias.size(); i++){
                                           		AccesoriosCategoria accaActual = acesoriosCategorias.get(i);
                                        	%> 	
                                        		<%if(i%10==0){%>
                                        			<div class="col-md-6 col-sm-6"><ul class="menu-items">
                                        		<%}%>
                                                  		<li><a href="<%=basePath%>listado.jsp?orden=1&pagina=1&acca=<%=accaActual.getId()%>"><%=accaActual.getNombre()%></a></li>  
                                                <%if(contador==10){contador=0;%>
                                        			</ul></div>
                                        		<%}%>
                                            <%contador++;}
                                			}%>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle">Celulares</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                                <%if(marcasConCelulares!=null){
                                	Integer contador =1;
                                      for(int i=0; i<marcasConCelulares.size(); i++){
                                           Marca marcaActual = marcasConCelulares.get(i);
                                        %>
                                        
                                        <%if(i%10==0){%>
                                        	<div class="col-md-6 col-sm-6"><ul class="menu-items">
                                        <%}%>
                                    			<li><a href="<%=basePath%>listado.jsp?orden=1&pagina=1&categoria=1&marca=<%=marcaActual.getId()%>"><%=marcaActual.getNombre()%></a></li>
                                    	<%if(contador==10){contador=0;%>
                                        	</ul></div>
                                        <%}%>
                                <%}
                                }%>
                                		</div>
                                   </div>
                                </ul>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle">Tablets</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                                <%if(marcasConTablets!=null){
                                	Integer contador =1;
                                      for(int i=0; i<marcasConTablets.size(); i++){
                                           Marca marcaActual = marcasConTablets.get(i);
                                        %>
                                        
                                        <%if(i%10==0){%>
                                        	<div class="col-md-6 col-sm-6"><ul class="menu-items">
                                        <%}%>
                                    			<li><a href="<%=basePath%>listado.jsp?orden=1&pagina=1&categoria=2&marca=<%=marcaActual.getId()%>"><%=marcaActual.getNombre()%></a></li>
                                    	<%if(contador==10){contador=0;%>
                                        	</ul></div>
                                        <%}%>
                                <%}
                                }%>
                                		</div>
                                   </div>
                                </ul>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle">Portatiles</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                                <%if(marcasConPortatiles!=null){
                                	Integer contador =1;
                                      for(int i=0; i<marcasConPortatiles.size(); i++){
                                           Marca marcaActual = marcasConPortatiles.get(i);
                                        %>
                                        
                                        <%if(i%10==0){%>
                                        	<div class="col-md-6 col-sm-6"><ul class="menu-items">
                                        <%}%>
                                    			<li><a href="<%=basePath%>listado.jsp?orden=1&pagina=1&categoria=3&marca=<%=marcaActual.getId()%>"><%=marcaActual.getNombre()%></a></li>
                                    	<%if(contador==10){contador=0;%>
                                        	</ul></div>
                                        <%}%>
                                <%}
                                }%>
                                		</div>
                                   </div>
                                </ul>
                            </li>
                            
                           	<li class="dropdown">
                                <a href="#" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle">Computadores</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                                <%if(marcasConComputadores!=null){
                                	Integer contador =1;
                                      for(int i=0; i<marcasConComputadores.size(); i++){
                                           Marca marcaActual = marcasConComputadores.get(i);
                                        %>
                                        
                                        <%if(i%10==0){%>
                                        	<div class="col-md-6 col-sm-6"><ul class="menu-items">
                                        <%}%>
                                    			<li><a href="<%=basePath%>listado.jsp?orden=1&pagina=1&categoria=4&marca=<%=marcaActual.getId()%>"><%=marcaActual.getNombre()%></a></li>
                                    	<%if(contador==10){contador=0;%>
                                        	</ul></div>
                                        <%}%>
                                <%}
                                }%>
                                		</div>
                                   </div>
                                </ul>
                            </li>
                            
                            <%}else{%>
                            	<li><a href="<%=basePath%>admin/menuAdministracion.jsp">Menu Administracion</a></li>
                            	<li><a href="<%=basePath%>logOutServlet">Salir</a></li>
                            <%}%>
                            
<!--                             <li class="dropdown"> -->
<!--                                 <a href="#" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle">Otros</a> -->
<!--                                 <ul class="dropdown-menu"> -->
<%--                                     <li><a href="<%=basePath%>index.jsp">Home Page</a></li> --%>
<%--                                     <li><a href="<%=basePath%>category.jsp">Category</a></li> --%>
<%--                                     <li><a href="<%=basePath%>detail.jsp">Individual Product</a></li> --%>
<%--                                     <li><a href="<%=basePath%>cart.jsp">Cart Summary</a></li> --%>
<%--                                     <li><a href="<%=basePath%>checkout.jsp">Checkout</a></li> --%>
<%--                                     <li><a href="<%=basePath%>404.jsp">404</a></li> --%>
<%--                                     <li><a href="<%=basePath%>contact.jsp">Contact</a></li> --%>
<!--                                 </ul> -->
<!--                             </li> -->
                            
<!--                             <li class="dropdown"> -->
<!--                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Celulares</a> -->
<!--                                 <ul class="dropdown-menu"> -->
<!--                                     <li> -->
<!--                                         <div class="yamm-content"> -->
<!--                                             <div class="row"> -->
<!--                                                 <div class="col-md-6 col-sm-6"> -->
<!--                                                     <span class="megamenu-title">Puppy Dogs</span> -->
<!--                                                     <ul class="menu-items"> -->
<%--                                                         <li><a href="<%=basePath%>category.jsp">Sit amet, consectetur</a></li> --%>
<%--                                                         <li><a href="<%=basePath%>category.jsp">Eiusmod Tempor</a></li> --%>
<%--                                                         <li><a href="<%=basePath%>category.jsp">Laudantium</a></li> --%>
<%--                                                         <li><a href="<%=basePath%>category.jsp">Veritatis Etseman</a></li> --%>
<!--                                                     </ul> -->
<!--                                                 </div> -->
<!--                                                 <div class="col-md-6 col-sm-6"> -->
<!--                                                     <span class="megamenu-title">Normal Dogs</span> -->
<!--                                                     <ul class="menu-items"> -->
<%--                                                         <li><a href="<%=basePath%>category.jsp">Sit amet, consectetur</a></li> --%>
<%--                                                         <li><a href="<%=basePath%>category.jsp">Eiusmod Tempor</a></li> --%>
<%--                                                         <li><a href="<%=basePath%>category.jsp">Laudantium</a></li> --%>
<%--                                                         <li><a href="<%=basePath%>category.jsp">Veritatis Etseman</a></li> --%>
<!--                                                     </ul> -->
<!--                                                 </div> -->
<!--                                             </div> -->
<!--                                         </div> -->
<!--                                     </li> -->
<!--                                 </ul> -->
<!--                             </li> -->
                        
<%--                             <li class="hidden-sm"><a href="<%=basePath%>category.jsp">French Bulldogs</a></li> --%>
<%--                             <li><a href="<%=basePath%>category.jsp">Pitbulls</a></li> --%>

<!--                             <li class="dropdown"> -->
<!--                                 <a href="#" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle">Pages</a> -->
<!--                                 <ul class="dropdown-menu"> -->
<%--                                     <li><a href="<%=basePath%>index.jsp">Home Page</a></li> --%>
<%--                                     <li><a href="<%=basePath%>category.jsp">Category</a></li> --%>
<%--                                     <li><a href="<%=basePath%>detail.jsp">Individual Product</a></li> --%>
<%--                                     <li><a href="<%=basePath%>cart.jsp">Cart Summary</a></li> --%>
<%--                                     <li><a href="<%=basePath%>checkout.jsp">Checkout</a></li> --%>
<%--                                     <li><a href="<%=basePath%>404.jsp">404</a></li> --%>
<%--                                     <li><a href="<%=basePath%>contact.jsp">Contact</a></li> --%>
<!--                                 </ul> -->
<!--                             </li> -->
                        </ul><!-- /.nav -->
                        
<!--                         <div class="navbar-right user-options"> -->
<!--                             <div class="btn-group"> -->
<!--                                 <button data-hover="dropdown" data-toggle="dropdown"  class="dropdown-toggle btn btn-primary-very-light navbar-btn">USD <b class="caret"></b></button> -->
<!--                                 <ul class="dropdown-menu"> -->
<!--                                     <li><a href="#">EUR</a></li> -->
<!--                                     <li><a href="#">INR</a></li> -->
<!--                                 </ul> -->
<!--                             </div>/.btn-group -->
<!--                             <div class="btn-group"> -->
<!--                                 <button data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle btn btn-primary-very-light navbar-btn">EN <b class="caret"></b></button> -->
<!--                                 <ul class="dropdown-menu"> -->
<!--                                     <li><a href="#">DE</a></li> -->
<!--                                     <li><a href="#">TA</a></li> -->
<!--                                 </ul>/.dropdown-menu -->
<!--                             </div>/.btn-group -->
<!--                         </div>/.navbar-right -->
                    </div><!-- /.collapse navbar-collapse -->
                </div><!-- /.container -->
            </nav><!-- /.yamm -->
        <!-- =================== NAVBAR PRIMARY :END ============================== -->
        </header><!-- /.header -->
        <!-- =================== HEADER :END ============================== -->
        
        <!-- =================== MODAL ============================== -->
        <div class="modal fade myCart-modal-lg" id="myCart" tabindex="-1" role="dialog" aria-hidden="true">
             <div class="modal-dialog">
                <div class="modal-content cart-modal-content">
                    <div class="modal-header">
                         <a class="close close-icon" data-dismiss="modal"><span data-icon="M"></span></a>
                         <h2 class="modal-title"><span class="icon glyphicon glyphicon-shopping-cart"></span>your cart-4 items</h2>
                    </div><!-- /.modal-header -->
                    <div class="modal-body">
                        <ul class="list-unstyled">
                            <li>
                                <div class="basket-item">
                                    <div class="row">
                                    	<div class="col-md-3 col-sm-3">
                                    		<div class="selected-item"><img src="<%=basePath%>assets/images/products/product-small-01.jpg" alt=""></div>
                                    		<div class="selected-item-price">$1000.00</div>
                                    	</div><!-- /.col-->
                                    	<div class="col-md-8 col-sm-8">
                                    		<h4>Adipiscing Elit,Sed do Eiusmod</h4>
                                    		<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do  eiusmod tempor incididunt ut labore</p> 
                                    	</div><!-- /.col -->
                                    	<div class="col-md-1 col-sm-1">
                                            <a class="cart-item-remove" title="cancel"  href="#">
                                                <span data-icon="M"></span>
                                            </a>
                                    	</div><!-- /.col -->
                                    </div><!-- /.row -->
                                </div><!-- /.basket-item -->
                            </li>

                            <li>
                                <div class="basket-item">
                                    <div class="row">
                                    	<div class="col-md-3 col-sm-3">
                                    		<div class="selected-item"><img src="<%=basePath%>assets/images/products/product-small-02.jpg" alt=""></div>
                                    		<div class="selected-item-price">$1000.00</div>
                                    	</div><!-- /.col -->
                                    	<div class="col-md-8 col-sm-8">
                                    		<h4>Do euismod tempor incididunt ut</h4>
                                    		<p> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud</p>
                                    	</div><!-- /.col -->
                                    	<div class="col-md-1 col-sm-1">
                                    		<a class="cart-item-remove" title="cancel" href="#">
                                                <span data-icon="M"></span>
                                            </a>
                                    	</div><!-- /.col -->
                                    </div><!-- /.row -->
                                </div><!-- /.basket-item -->
                            </li>
                        </ul><!-- /.list-unstyled -->

                        <div class="row">
                        	<div class="product-actions">
                        		<a  href="<%=basePath%>detail.jsp" class="btn btn-secondary btn-ky">Total:$2000.00</a>
                        		<a href="<%=basePath%>checkout.jsp" class="btn btn-inverse btn-ky">Continued to checkout</a>
                        	</div><!-- /.product-actions -->
                        </div><!-- /.row -->
                    </div><!-- /.modal-body -->
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- =================== MODAL : END ============================== -->
	
	<decorator:body />
	
	
			<!-- ============================= FOOTER ============================-->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12">
        <!-- ========================== FOLLOW US FOOTER =================== -->
                        <div class="module widget">
                            <div class="module-header">
                                <h4 class="module-title text-md-center">Síguenos</h4>
                            </div><!-- /.module-header -->
                            <div class="module-body text-md-center">
                                <div class="row">
                                    <div class="col-md-12 col-sm-6">
                                        <p>Síguenos en Facebook</p>
                                        <ul class="list-social-icons">
                                            <li><a href="https://www.facebook.com/mundoaccesoriosstore/?fref=ts"><span data-icon="&#xe093;"></span></a></li>
<!--                                             <li><a href="#"><span data-icon="&#xe094;"></span></a></li> -->
<!--                                             <li><a href="#"><span data-icon="&#xe095;"></span></a></li> -->
<!--                                             <li><a href="#"><span data-icon="&#xe096;"></span></a></li> -->
<!--                                             <li><a href="#"><span data-icon="&#xe09b;"></span></a></li> -->
                                        </ul><!-- /.list-social-icons -->
                                    </div><!-- /.col -->
                                    <div class="col-md-12 col-sm-6">
                                        <address>
                                            <strong>Estamos ubicados en:</strong><br/>
                                            C.C Parque Central Local 190 y 198 'A', Barranquilla, Colombia <br/>
                                            (+57)(5)3177864
                                        </address>  
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div><!-- /.module-body -->
                        </div><!-- /.module -->
        <!-- ========================= FOLLOW US FOOTER :END ================== -->         
                    </div><!-- /.col -->

                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-6">
        <!-- ========================== NEWSLETTER FOOTER ====================== -->
                                <div class="module block-newsletter">
                                    <div class="module-header">
                                        <h4 class="module-title text-md-center">Promociones</h4>
                                    </div><!-- /.module-header -->
                                    <div class="module-body text-md-center">
                                        <p>Registra tu email y recibe las mejores promociones.</p>
                                        <form role="form">
                                            <div class="input-group">
                                                <input id="emailCliente" type="text" class="form-control" placeholder="tucorreo@email.com" autocomplete="off">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" 
                                                    onclick="guardarEmailCliente(document,'<%=basePath%>registrarClienteEmailServlet')"><span data-icon="&#x45;"></span></button>
                                                </span>
                                            </div><!-- /input-group -->
                                        </form>
                                    </div><!-- /.module-body -->
                                </div><!-- /.module -->
        <!-- ========================= NEWSLETTER FOOTER :END ==================== -->                
                            </div><!-- /.col -->
                            <div class="col-md-12 col-sm-6">
        <!-- ============================= PAYMENT FOOTER ======================== -->
                                <div class="module widget">
                                    <div class="module-header">
                                        <h4 class="module-title text-md-center">Medios de pago</h4>
                                    </div><!-- /.module-header -->
                                    <div class="module-body text-md-center">
                                        <p>Nuestros medios de pago son</p>
                                        <ul class="list-payment-logos">
                                        	<li><img src="<%=basePath%>assets/images/payments/cash.png" alt="Efectivo" title="Efectivo" width="48" style="margin-top: -7px;" /></li>
                                            <li><img src="<%=basePath%>assets/images/payments/visa.png" alt="Visa" title="Visa" width="48" height="32" /></li>
                                            <li><img src="<%=basePath%>assets/images/payments/mastercard.png" alt="MasterCard" title="MasterCard" width="48" height="32" /></li>
                                            <li><img src="<%=basePath%>assets/images/payments/american-express.png" alt="American Express" title="American Express" width="48" height="32" /></li> 
<%--                                        <li><img src="<%=basePath%>assets/images/payments/paypal.png" alt="Paypal" width="48" height="32" /></li> --%>
                                        </ul><!-- /.payment-logo-list -->
                                    </div><!-- /.module-body -->
                                </div><!-- /.module -->
        <!-- ============================= PAYMENT FOOTER : END ====================== -->                
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.col -->

                    <div class="col-md-4 col-sm-12 col-xs-12">
        <!-- ==============================SHIPPING DELIVERY FOOTER =================== -->
<!--                         <div class="module"> -->
<!--                             <div class="module-header"> -->
<!--                                 <h4 class="module-title text-md-center">Shipping &amp; Delivery</h4> -->
<!--                             </div>/.module-header -->
<!--                             <div class="module-body text-md-center"> -->
<!--                                 <p>Lorem ipsum dolor sit amet</p> -->
<!--                                 <ul class="list-link"> -->
<%--                                     <li><a href="<%=basePath%>category.jsp">Consectetur adipisicing elit, sed do eiusmod</a></li> --%>
<%--                                     <li><a href="<%=basePath%>category.jsp">Tempor incididunt ut labore et dolore</a></li> --%>
<%--                                     <li><a href="<%=basePath%>category.jsp">Magna aliqua. Ut enim ad minim</a></li> --%>
<%--                                     <li><a href="<%=basePath%>category.jsp">Veniam, quis nostrud exercitation</a></li> --%>
<%--                                     <li><a href="<%=basePath%>category.jsp">Ullamco laboris nisi ut aliquip ex ea</a></li> --%>
<!--                                 </ul>/.list-link -->
<!--                             </div>/.module-body -->
<!--                         </div>/.module -->
        <!-- ============================SHIPPING DELIVERY FOOTER : END ==================== -->         
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container -->

            <div class="navbar navbar-footer">
        <!-- =============================== NAVBAR FOOTER =================================== -->
                <div class="container text-md-center">
                	<ul class="navbar-nav nav hidden-sm hidden-xs">
                		<li><a href="<%=basePath%>index.jsp">Inicio</a></li>
                		<li><a href="<%=basePath%>admin/login.jsp">administración</a></li>
<%--                 		<li><a href="<%=basePath%>contact.jsp">Contacto</a></li> --%>
<%--                 		<li><a href="<%=basePath%>checkout.jsp">Shipping</a></li> --%>
<%--                 		<li><a href="<%=basePath%>contact.jsp">FAQs</a></li> --%>
                	</ul><!-- /.navbar-nav -->
                	<p class="navbar-text pull-right text-md-center copyright-text">Mundo Accesorios</p>
                </div><!-- /.container -->
        <!-- =========================== NAVBAR FOOTER : END ================================ -->
            </div><!-- /.navbar -->
    
        </footer>
        <!-- ================================ FOOTER :END ===================================== -->
        
        <div id="divMiniMensaje" class="estiloMiniMensaje">
			<p id="textoMiniMensaje" style="margin: 5px;">...</p>
		</div>
        
    </div><!-- /.wrapper -->
	
	<!-- JavaScripts placed at the end of the document so the pages load faster -->
	<script src="<%=basePath%>assets/js/bootstrap.min.js"></script>
	<script src="<%=basePath%>assets/js/bootstrap-select.min.js"></script>
	<script src="<%=basePath%>assets/js/bootstrap-hover-dropdown.min.js"></script>
	<script src="<%=basePath%>assets/js/bootstrap-slider.js"></script>
	<script src="<%=basePath%>assets/js/echo.min.js"></script>
	<script src="<%=basePath%>assets/js/jquery.easing-1.3.min.js"></script>
	<script src="<%=basePath%>assets/js/flatui-controls.js"></script>
	<script src="<%=basePath%>assets/js/owl.carousel.min.js"></script>
	<script src="<%=basePath%>assets/js/wow.min.js"></script>
	<script src="<%=basePath%>assets/js/scripts.js"></script>
</body>
</html>