<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";
%>

<!DOCTYPE html>
	<html>
	<head>
		
    </head>
<body>

		<div class="content content-404">
	         <div class="container">
		<!-- ========================================= CONTENT ========================================= -->
        		<div class="info-404 text-center">
        			<h1>404</h1>
        			<img src="<%=basePath%>assets/images/bone.png" alt="404 Not Found">
        			<p>Sorry Guys! Page not available</p>
        			<p><a href="#" class="secondary-link">Sitemap</a> &ndash; <a href="<%=basePath%>index.jsp" class="primary-link">Home</a></p>
        		</div><!-- /.info-404 -->
		<!-- ========================================= CONTENT : END  ========================================= -->
	         </div><!-- /.container -->
        </div><!-- /.content -->

</body>
</html>