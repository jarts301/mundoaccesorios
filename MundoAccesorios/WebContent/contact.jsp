
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";
%>
<!DOCTYPE html>
	<html>
	<head>

    </head>
<body>

		<div class="content">
        <!-- ========================================= CONTENT ========================================= -->
            <div class="container">
                <h2 class="page-title">Contactanos</h2>
                <p class="page-subtitle">Puedes enviarnos un mensaje, y te contestaremos en el menor tiempo posible.</p>
            </div><!-- /.container -->

            <div class="page-section margin-top-40 contact-us-page-section">
                <iframe id="google-maps" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJgT7CzGUt9I4RVVWNqUsynJk&key=AIzaSyBI4fN_gAfPajqBa_xCS7RAzmypv55OdzI"></iframe>

                <div class="contact-us-form" style="opacity: 0.85;">
                    <div class="container">
                        <div class="row">
                            <div class="contact-us-form-section col-md-5 col-sm-6 col-sm-offset-7">
                                <div class="page-section no-margin-top">
                                    <h3 class="section-title">
                                        Envíanos un mensaje
                                    </h3>
                                    <form role="form">
                                        <div class="form-group">
                                            <label for="full-name" class="sr-only">Nombre</label>
                                            <input type="text" class="form-control" id="full-name" placeholder="Nombre">
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="subject" class="sr-only">Email</label>
                                            <input type="text" class="form-control" id="subject" placeholder="Email">
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="message" class="sr-only">Mensaje</label>
                                            <textarea id="message" class="form-control" placeholder="Mensaje"></textarea>
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
<!--                                             <label for="i-am-human" class="checkbox"><input type="checkbox" id="i-am-human"> Soy humano</label> -->
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-lg btn-block">Enviar</button>
                                        </div><!-- /.form-group -->
                                    </form><!-- /form -->
                                </div><!-- /.page-section -->
                            </div><!-- /.contact-us-form-section -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.contact us form -->  
                
            </div><!-- /.page-section -->
        <!-- ========================================= CONTENT : END ========================================= -->
        </div><!-- /.content -->

</body>
</html>