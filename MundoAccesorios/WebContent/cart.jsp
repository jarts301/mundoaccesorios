<%@page import="java.util.Iterator"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaProducto"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="jarts.mundoaccesorios.entidades.Producto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";

	DecimalFormat formatoNumeros = new DecimalFormat("###,###.##");
	ConsultaProducto consultaProducto= new ConsultaProducto();
	String cotizacion = (String)request.getSession().getAttribute("cotizacion");
	
	HashMap<Integer, Integer> hmProductos=null;
	ArrayList<Producto> productosCotizacion=null;
	Integer cantidadProductosCot=0;
	
	if(cotizacion!=null){
		cantidadProductosCot=cotizacion.substring(0,cotizacion.length()-1).split(",").length;
		hmProductos = consultaProducto.hacerMapCotizacion(
				cotizacion.substring(0,cotizacion.length()-1).split(","));
		productosCotizacion = consultaProducto.consultarProductos(hmProductos);
	}
%>

<!DOCTYPE html>
	<html>
	<head>
	
    </head>
<body>

		<div class="content">
             <div class="container">
        <!-- ========================================= CONTENT ========================================= -->
                <h2 class="page-title text-md-center">Cotización (<%=cantidadProductosCot%> Producto<%=cantidadProductosCot>1?"s":""%>)</h2>
                <p class="page-subtitle text-md-center">Aquí puedes consultar los productos que agregaste a la cotización</p>

<!--                 <div class="page-section"> -->
<!--                     <ul class="order-steps nav nav-pills nav-justified"> -->
<%--                         <li class="active"><a href="<%=basePath%>cart.jsp">Review Cart</a></li> --%>
<%--                         <li><a href="<%=basePath%>checkout.jsp">Confirm Payment</a></li> --%>
<!--                         <li><a href="#">Finish</a></li> -->
<!--                     </ul>/.order steps -->
<!--                 </div>/.page section  -->

                <div class="page-section">
                    <div class="row">
                        <div class="col-md-8">
                           <div class="table-responsive">
                                <table class="table table-cart-summary">
                                    <thead>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Descripcion</th>
                                            <th>Cantidad</th>
                                            <th>Precio Unitario</th>
                                            <th></th>
                                        </tr><!-- /tr -->
                                    </thead><!-- /thead -->
                                    <tfoot>
                                        <tr>
                                            <td colspan="2">
                                                <form role="form" class="form-coupon-code">
                                                    <div class="input-group">
                                                    <%if(cotizacion!=null){%>
                                                    	<button class="btn btn-primary btn-block btn-lg" 
                                                    	onclick="window.location='<%=basePath%>imprimirCotizacionServlet'"
                                                    	type="button">Imprimir cotización</button>
<!--                                                         <input type="text" class="form-control" placeholder="Coupon Code" autocomplete="off"> -->
<!--                                                         <span class="input-group-btn"> -->
<!--                                                             <button class="btn btn-primary" type="button">OK</button> -->
<!--                                                         </span> -->
													<%} %>
                                                    </div>
                                                </form>
                                            </td>
                                            <td colspan="2">
                                                <p class="final-total text-right">
                                                    <span class="final-total-text">Total:</span>
                                                    <span class="final-total-value"
                                                    >$<%
                        								Integer suma = 0;
                        								if(hmProductos!=null ){
                        									Iterator<Integer> keySetIterator = hmProductos.keySet().iterator(); 
                        									while(keySetIterator.hasNext()){ 
                        										Integer key = keySetIterator.next();
                        										suma = suma + (hmProductos.get(key)*consultaProducto.obtenerProducto(productosCotizacion, key).getPrecio());
                        									}
                        								}
                        								out.print(formatoNumeros.format(suma));
                        							%></span>
                                                </p>
                                            </td>
                                            <td></td>
                                        </tr><!-- /tr -->
                                    </tfoot><!-- /tfoot -->
                                    <tbody>
                                    
                                    <%if(productosCotizacion!=null){
                                    for(int i =0; i< productosCotizacion.size(); i++){
                                    %>
                                        <tr>
                                            <td colspan="2">
                                                <div class="media">
                                                    <a class="pull-left" href="<%=basePath%>product.jsp?producto=<%=productosCotizacion.get(i).getId()%>"><img src="<%=productosCotizacion.get(i).getImagen()!=null?basePath+"cargarImagenServlet?imagen="+productosCotizacion.get(i).getImagen():""%>" 
                                                    alt="Puppy Bully" height="112" width="112" class="media-object"></a>
                                                    <div class="media-body">
                                                        <h4 class="product-title"><%=productosCotizacion.get(i).getNombre()%></h4>
                                                        <p class="price">$<%=formatoNumeros.format(productosCotizacion.get(i).getPrecio())%></p>
                                                    </div><!-- /.media-body -->
                                                </div><!-- /.media -->
                                            </td>
                                            <td>
                                                <div class="cart-item-quantity">
                                                    <div class="input-group" style="text-align: center;">
<!--                                                         <span class="input-group-btn"> -->
<!--                                                             <button class="btn btn-default" type="button"><span data-icon="&#x4b;"></span></button> -->
<!--                                                         </span> -->
                                                        <span class="form-control" style="border-style: hidden;">
                                                        	<%=hmProductos.get(productosCotizacion.get(i).getId())%>
                                                        </span>
<!--                                                         <span class="input-group-btn"> -->
<!--                                                             <button class="btn btn-default" type="button"><span data-icon="&#x4c;"></span></button> -->
<!--                                                         </span> -->
                                                    </div><!-- /.input-group -->
                                                </div>
                                            </td>
                                            <td>
                                                <span class="unit-price">$<%=formatoNumeros.format(productosCotizacion.get(i).getPrecio())%></span>
                                            </td>
                                            <td><a onclick="eliminarProducto(document,'<%=basePath%>eliminarProductoCotizacionServlet?idProducto=<%=productosCotizacion.get(i).getId()%>')" class="cart-item-remove"><span data-icon="&#x4d;"></span></a></td>
                                        </tr><!-- /tr -->
                                     <%}
                                    } %>
                                        
                                    </tbody><!-- /tbody -->
                                </table><!-- /.table -->
                            </div><!-- /.table responsive --> 
                        </div><!-- /.col -->
<!--                         <div class="col-md-4 authentication"> -->
<!--                             <div class="page-section no-margin-top"> -->
<!--                                 <h3 class="section-title">Register Checkout</h3>     -->
<!--                                 <form role="form"> -->
<!--                                     <div class="form-group"> -->
<!--                                         <label for="email" class="sr-only">Email Address</label> -->
<!--                                         <input type="text" id="email" class="form-control input-lg" placeholder="Email"> -->
<!--                                     </div>/.form-group -->

<!--                                     <div class="form-group"> -->
<!--                                         <label for="password" class="sr-only">Password</label> -->
<!--                                         <input type="text" id="password" class="form-control input-lg" placeholder="Password"> -->
<!--                                     </div>/.form-group -->

<!--                                     <div class="form-group"> -->
<!--                                         <label class="checkbox label-lg"><input type="checkbox"> Remember me</label> -->
<!--                                     </div>/.form-group -->
                                    
<!--                                     <div class="form-group"> -->
<!--                                         <button class="btn btn-primary btn-block btn-lg" type="submit">Sign In and Checkout</button> -->
<!--                                     </div>/.form-group -->
<!--                                 </form> -->
<!--                             </div>/.page section  -->
<!--                             <div class="page-section"> -->
<!--                                 <h3 class="section-title">New Customer</h3> -->
<!--                                 <form role="form"> -->
<!--                                     <div class="form-group"> -->
<!--                                         <label for="create-new-account" class="label-lg radio"><input name="new-customer" id="create-new-account" type="radio"/> Create a new account</label> -->
<!--                                         <label for="checkout-guest" class="label-lg radio"><input name="new-customer" id="checkout-guest" type="radio"/> Checkout as Guest</label> -->
<!--                                     </div>/.form-group -->
<!--                                     <div class="form-group"> -->
<!--                                         <button class="btn btn-primary btn-block btn-lg" type="submit">Continue Checkout</button> -->
<!--                                     </div>/.form-group -->
<!--                                 </form> -->
<!--                             </div>/.page section  -->
<!--                         </div>/.col -->
                    </div><!-- /.row-->
                </div><!-- /.page-section -->

        <!-- ========================================= CONTENT : END ========================================= -->
            </div><!-- /.container -->
        </div><!-- /.content -->

</body>
</html>