<%@page import="java.text.DecimalFormat"%>
<%@page import="jarts.mundoaccesorios.general.dao.ConsultaProducto"%>
<%@page import="jarts.mundoaccesorios.entidades.Producto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";

	DecimalFormat formatoNumeros = new DecimalFormat("###,###.##");

	Integer idProducto = Integer.parseInt(request.getParameter("producto"));
	
	ConsultaProducto consultaProducto = new ConsultaProducto();
	Producto producto= consultaProducto.consultarProducto(idProducto);

%>

<!DOCTYPE html>
<html>
	<head>
 
	</head>
<body>

        <div class="content">
            <div class="container">
                <div class="row">
            <!-- ========================================================= CONTENT ============================================================= -->
                    <div class="col-md-9 col-md-push-3">
                        <div class="featured-product margin-top-40">
                        <%if(producto!=null){%>
                            <div class="row">
                                <div class="col-md-5 col-sm-5">
                                    <h2 class="page-title no-margin-top text-md-center"><%=producto.getNombre()%></h2>
                                    <p class="page-subtitle text-md-center"><%=producto.getDescripcion()!=null?producto.getDescripcion():""%></p>
                                    <table class="table-product-price">
                                        <tr class="row">
                                            <td class="col-md-5 col-sm-5">
                                                <span class="price">$<%=formatoNumeros.format(producto.getPrecio())%></span>
                                            </td>
<!--                                             <td class="col-md-2 col-sm-2 col-separator-td"> -->
<!--                                                 <span class="col-separator">&nbsp;</span> -->
<!--                                             </td> -->
<!--                                             <td class="col-md-5 col-sm-5 old-price-container"> -->
<!--                                                 was <br/> -->
<!--                                                 <span class="old-price">$1200.00</span> -->
<!--                                             </td> -->
                                        </tr>
                                    </table><!-- /table -->
                                    <div class="product-actions hidden-xs hidden-sm">
                                        <a class="btn btn-add-to-cart btn-secondary btn-ky" onclick="mensajeCotizacion(document,this,'<%=basePath%>cart.jsp');cotizarProducto(document,'<%=producto.getId()%>','<%=producto.getPrecio()%>','<%=basePath%>cotizarProductoServlet')">
                                        <img src="<%=basePath%>assets/images/icon-cart.png" alt="" class="icon-btn" /> Cotizar</a>
                                        <a href="<%=producto.getLink()!=null?producto.getLink():""%>" class="btn btn-inverse btn-ky">Más<span data-icon="&#x35;"></span></a>
                                    </div><!-- /.product-actions -->
                                </div><!-- /.col -->

                                <div class="col-md-7 col-sm-7">
                                    <img src="<%=basePath%>assets/images/blank.gif" data-echo="<%=producto.getImagen()!=null?basePath+"cargarImagenServlet?imagen="+producto.getImagen():""%>" alt="Cute French Bully" width="590" class="img-responsive">
                                </div><!-- /.col -->

                                <div class="col-md-12 col-sm-12 hidden-lg hidden-md">
                                    <div class="product-actions margin-top-20 text-center">
                                        <a class="btn btn-add-to-cart btn-secondary btn-ky" onclick="mensajeCotizacion(document,this,'<%=basePath%>cart.jsp');cotizarProducto(document,'<%=producto.getId()%>','<%=producto.getPrecio()%>','<%=basePath%>cotizarProductoServlet')">
                                        <img src="<%=basePath%>assets/images/icon-cart.png" alt="" class="icon-btn" /> Cotizar</a>
                                        <a href="<%=producto.getLink()!=null?producto.getLink():""%>" class="btn btn-inverse btn-ky">Más<span data-icon="&#x35;"></span></a>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                            <%}%>
                            <hr />
                        </div><!-- /.featured-product -->

                    </div><!-- /.col-md-9 -->
            <!-- ========================================= CONTENT : END ========================================= -->    

            <!-- ========================================= SIDEBAR ========================================= -->
                    <div class="col-md-3 col-md-pull-9">

                    </div><!-- /.col-md-3 -->
            <!-- ========================================= SIDEBAR : END ========================================= -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.content -->

</html>