package jarts.mundoaccesorios.admin.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Producto;
import jarts.mundoaccesorios.general.dao.ConsultaProducto;

public class EliminaProducto {

	private Connection conexion;
	private DatosConexion datosConexion;

	public Integer eliminarProducto(Integer idProducto) {
		Integer res = -1;

		String query = "delete from producto where producto_id = " + idProducto;

		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			consulta.executeUpdate(query);

			consulta.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			res = -1;
		}
		return res;
	}

	public Integer eliminarProductosACCA(Integer idCategoria) {
		Integer res = -1;

		String query = "delete from producto where acca_id = " + idCategoria;

		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			consulta.executeUpdate(query);

			consulta.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			res = -1;
		}
		return res;
	}

	public Integer eliminarProductosMarca(Integer idMarca) {
		Integer res = -1;

		String query = "delete from producto where marca_id = " + idMarca;

		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			consulta.executeUpdate(query);

			consulta.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			res = -1;
		}
		return res;
	}

	public void eliminarImagenProducto(Integer idProducto) {
		DatosConexion datosConexion = new DatosConexion();
		ConsultaProducto consultaProducto = new ConsultaProducto();
		Producto producto = consultaProducto.consultarProducto(idProducto);

		File file = new File(datosConexion.getPropiedad("carpeta.imagenes") + producto.getImagen());
		file.delete();
	}

}
