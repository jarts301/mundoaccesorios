package jarts.mundoaccesorios.admin.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import jarts.mundoaccesorios.conexion.DatosConexion;

public class EliminaMarca {
	
	private Connection conexion;
	private DatosConexion datosConexion;

	public Integer eliminarMarca(Integer idMarca) {
		Integer res=-1;
		
		String query = "delete from marca where marca_id = "+idMarca;
				
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			consulta.executeUpdate(query);
			
			consulta.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			res=-1;
		}
		return res;
	}

}
