package jarts.mundoaccesorios.admin.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Cliente;

public class ConsultaCorreos {
	
	private Connection conexion;
	private DatosConexion datosConexion;
	
	public String consultarCorreos() {
		
		String query = "select cliente_email from cliente where cliente_rol <> 'administrador'";
		String cadena= "";

		Cliente cliente = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			while (resultado.next()) {
				cadena=cadena+resultado.getString(1)+"; ";
			}
			
			if(!cadena.equals("")){
				cadena=cadena.substring(0,cadena.length()-2);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			cliente=null;
			e.printStackTrace();
		}
		
		return cadena;
	}
}
