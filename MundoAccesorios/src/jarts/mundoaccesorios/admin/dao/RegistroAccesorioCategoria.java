package jarts.mundoaccesorios.admin.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.AccesoriosCategoria;

public class RegistroAccesorioCategoria {
	
	private Connection conexion;
	private DatosConexion datosConexion;

	public Integer registrarAccCategoria(AccesoriosCategoria accCategoria) {
		Integer res=-1;
		
		String query = "insert into accesorios_categoria(acca_nombre) " + "values('"+accCategoria.getNombre()+"')";
		
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			consulta.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
			
			ResultSet resultSet = consulta.getGeneratedKeys();
			
			while (resultSet.next()) {
				res=resultSet.getInt(1);
			}

			consulta.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			res=-1;
		}
		return res;
	}

}
