package jarts.mundoaccesorios.admin.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Producto;

public class ActualizaProducto {
	
	private Connection conexion;
	private DatosConexion datosConexion;

	public Integer actualizarProducto(Producto producto) {
		Integer res=-1;
		
		String query = "update producto set ";
		
		query=query+"producto_precio = "+producto.getPrecio()+",";
		
		if(producto.getDescripcion()!=null){
			query=query+"producto_descripcion = "+"'"+producto.getDescripcion()+"',";
		}

		if(producto.getLink()!=null){
			query=query+"producto_link = "+"'"+producto.getLink()+"',";
		}
		
		query=query+"categoria_id = "+producto.getIdCategoria()+",";
		query=query+"marca_id = "+producto.getIdMarca()+",";
		
		if(producto.getIdAccesoriosCategoria()!=null && producto.getIdAccesoriosCategoria()!=0){
			query=query+"acca_id = "+producto.getIdAccesoriosCategoria()+",";
		}
		query=query.substring(0,query.length()-1);
		
		query=query +" where producto_id="+producto.getId();
				
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			consulta.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
			
			ResultSet resultSet = consulta.getGeneratedKeys();
			
			while (resultSet.next()) {
				res=resultSet.getInt(1);
			}

			consulta.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			res=-1;
		}
		return res;
	}
	
	public Integer destacarProducto(Producto producto, Boolean valor) {
		Integer res=-1;
		
		String query = "update producto set producto_destacado="+((valor)?1:0)+" where producto_id="+producto.getId();
				
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			consulta.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
			
			ResultSet resultSet = consulta.getGeneratedKeys();
			
			while (resultSet.next()) {
				res=resultSet.getInt(1);
			}

			consulta.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			res=-1;
		}
		return res;
	}

}
