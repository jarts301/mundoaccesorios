package jarts.mundoaccesorios.admin.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Slider;

public class ActualizaSlide {
	
	private Connection conexion;
	private DatosConexion datosConexion;

	public Integer actualizarSlide(Slider slider) {
		Integer res=-1;
		
		String query = "update slider set ";
		
		if(slider.getNombre()!=null && !slider.getNombre().equals("")){
			query=query+"slider_nombre = "+"'"+slider.getNombre()+"',";
		}
		if(slider.getDescripcion()!=null && !slider.getDescripcion().equals("")){
			query=query+"slider_descripcion = "+"'"+slider.getDescripcion()+"',";
		}

		if(slider.getLink()!=null && !slider.getLink().equals("")){
			query=query+"slider_link = "+"'"+slider.getLink()+"',";
		}
		
		query=query+"slider_posicion = "+slider.getPosicion()+",";
		
		query=query.substring(0,query.length()-1);
		
		query=query +" where slider_id="+slider.getId();
				
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			consulta.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
			
			ResultSet resultSet = consulta.getGeneratedKeys();
			
			while (resultSet.next()) {
				res=resultSet.getInt(1);
			}

			consulta.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			res=-1;
		}
		return res;
	}

}
