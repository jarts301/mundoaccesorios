package jarts.mundoaccesorios.admin.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Slider;
import jarts.mundoaccesorios.general.dao.ConsultaSlider;

public class EliminaSlide {
	
	private Connection conexion;
	private DatosConexion datosConexion;

	public Integer eliminarSlide(Integer sliderId) {
		Integer res=-1;
		
		String query = "delete from slider where slider_id = "+sliderId;
				
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			consulta.executeUpdate(query);
			
			consulta.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			res=-1;
		}
		return res;
	}
	
	public void eliminarImagenSlider(Integer idSlider){
		DatosConexion datosConexion = new DatosConexion();
		ConsultaSlider consultaSlider = new ConsultaSlider();
		Slider slider=consultaSlider.consultarSlider(idSlider);
		
		File file = new File(datosConexion.getPropiedad("carpeta.sliders")+slider.getImagen());
		file.delete();
	}

}
