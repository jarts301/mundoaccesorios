package jarts.mundoaccesorios.admin.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Producto;

public class RegistroProducto {
	
	private Connection conexion;
	private DatosConexion datosConexion;

	public Integer registrarProducto(Producto producto) {
		Integer res=-1;
		
		SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String cadenaAtributos="producto_nombre,producto_precio,";
		String cadenaParametros="'"+producto.getNombre()+"',"+producto.getPrecio()+",";
		
		if(producto.getDescripcion()!=null){
			cadenaAtributos=cadenaAtributos+"producto_descripcion,";
			cadenaParametros=cadenaParametros+"'"+producto.getDescripcion()+"',";
		}
		
		cadenaAtributos=cadenaAtributos+"producto_imagen,";
		cadenaParametros=cadenaParametros+"'"+producto.getImagen()+"',";

		if(producto.getLink()!=null){
			cadenaAtributos=cadenaAtributos+"producto_link,";
			cadenaParametros=cadenaParametros+"'"+producto.getLink()+"',";
		}
		
		cadenaAtributos=cadenaAtributos+"producto_fecha,categoria_id,marca_id,";
		cadenaParametros=cadenaParametros+ "STR_TO_DATE('" + formatoFecha.format(producto.getFechaRegistro()) + "', '%Y-%m-%d %H:%i:%s'),"+producto.getIdCategoria()+","+producto.getIdMarca()+",";
		
		if(producto.getIdAccesoriosCategoria()!=null && producto.getIdAccesoriosCategoria()!=0){
			cadenaAtributos=cadenaAtributos+"acca_id,";
			cadenaParametros=cadenaParametros+producto.getIdAccesoriosCategoria()+",";
		}
		cadenaAtributos=cadenaAtributos.substring(0,cadenaAtributos.length()-1);
		cadenaParametros=cadenaParametros.substring(0,cadenaParametros.length()-1);
		
		String query = "insert into producto("+cadenaAtributos+") " + "values("+cadenaParametros+")";
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			consulta.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
			
			ResultSet resultSet = consulta.getGeneratedKeys();
			
			while (resultSet.next()) {
				res=resultSet.getInt(1);
			}

			consulta.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			res=-1;
		}
		return res;
	}

}
