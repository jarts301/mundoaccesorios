package jarts.mundoaccesorios.admin.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Producto;
import jarts.mundoaccesorios.entidades.Slider;

public class RegistroSlide {
	
	private Connection conexion;
	private DatosConexion datosConexion;

	public Integer registrarSlide(Slider slider) {
		Integer res=-1;
		
		String cadenaAtributos="";
		String cadenaParametros="";
		
		if(slider.getNombre()!=null){
			cadenaAtributos=cadenaAtributos+"slider_nombre,";
			cadenaParametros=cadenaParametros+"'"+slider.getNombre()+"',";
		}
		if(slider.getDescripcion()!=null){
			cadenaAtributos=cadenaAtributos+"slider_descripcion,";
			cadenaParametros=cadenaParametros+"'"+slider.getDescripcion()+"',";
		}
		
		cadenaAtributos=cadenaAtributos+"slider_imagen,";
		cadenaParametros=cadenaParametros+"'"+slider.getImagen()+"',";

		if(slider.getLink()!=null){
			cadenaAtributos=cadenaAtributos+"slider_link,";
			cadenaParametros=cadenaParametros+"'"+slider.getLink()+"',";
		}
		
		cadenaAtributos=cadenaAtributos+"slider_posicion,";
		cadenaParametros=cadenaParametros+slider.getPosicion()+",";

		cadenaAtributos=cadenaAtributos.substring(0,cadenaAtributos.length()-1);
		cadenaParametros=cadenaParametros.substring(0,cadenaParametros.length()-1);
		
		String query = "insert into slider("+cadenaAtributos+") " + "values("+cadenaParametros+")";
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			consulta.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
			
			ResultSet resultSet = consulta.getGeneratedKeys();
			
			while (resultSet.next()) {
				res=resultSet.getInt(1);
			}

			consulta.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			res=-1;
		}
		return res;
	}

}
