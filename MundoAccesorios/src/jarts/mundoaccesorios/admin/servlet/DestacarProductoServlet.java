package jarts.mundoaccesorios.admin.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.admin.dao.ActualizaProducto;
import jarts.mundoaccesorios.entidades.Producto;
import jarts.mundoaccesorios.general.dao.ConsultaProducto;
import jarts.mundoaccesorios.general.dao.Utilidades;

public class DestacarProductoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");

			Integer productoId = Integer.parseInt(Utilidades.escapeHtml(req.getParameter("idProducto")));
			
			ConsultaProducto consultaProducto = new ConsultaProducto();
			Producto producto= consultaProducto.consultarProducto(productoId);
			
			ActualizaProducto actualizaProducto = new ActualizaProducto();
			actualizaProducto.destacarProducto(producto,!producto.getDestacado());
			
			resp.sendRedirect("listado.jsp?orden=1&pagina=1");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
