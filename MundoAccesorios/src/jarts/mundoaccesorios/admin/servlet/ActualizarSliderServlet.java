package jarts.mundoaccesorios.admin.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.admin.dao.ActualizaProducto;
import jarts.mundoaccesorios.admin.dao.ActualizaSlide;
import jarts.mundoaccesorios.entidades.Producto;
import jarts.mundoaccesorios.entidades.Slider;
import jarts.mundoaccesorios.general.dao.Utilidades;

public class ActualizarSliderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");

			Integer sliderId = Integer.parseInt(Utilidades.escapeHtml(req.getParameter("hidSlider")));
			String nombre = Utilidades.escapeHtml(req.getParameter("nombre"));
			String descripcion = Utilidades.escapeHtml(req.getParameter("descripcion"));
			String link = Utilidades.escapeHtml(req.getParameter("link"));
			String posicion = Utilidades.escapeHtml(req.getParameter("posicion"));
			
			Slider slider= new Slider();
			slider.setId(sliderId);
			slider.setNombre(nombre);
			slider.setDescripcion(descripcion);
			slider.setLink(link);
			slider.setPosicion(Integer.parseInt(posicion));
			
			ActualizaSlide actualizaSlide = new ActualizaSlide();
			actualizaSlide.actualizarSlide(slider);
			
			resp.sendRedirect("admin/listadoSliders.jsp");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
