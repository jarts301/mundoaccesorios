package jarts.mundoaccesorios.admin.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import jarts.mundoaccesorios.admin.dao.RegistroSlide;
import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Slider;
import jarts.mundoaccesorios.general.dao.Utilidades;
import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;

public class RegistrarSlideServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	MultipartFormDataRequest mrequest = null;
	UploadBean upBean = null;
	UploadFile file;
	String ubicacionArchivo = "";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");
			
			Slider slider = new Slider();

			String nombre = "";
			String descripcion="";
			String link="";
			String posicion="";
			
			String nombreImagen="";
			String extension = ".jpg";

			if (ServletFileUpload.isMultipartContent(req)) {
				try {
					List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(req);

					for (FileItem item : multiparts) {

						if (item.getFieldName().equals("nombre")) {
							nombre = Utilidades.escapeHtml(new String(getStringFromInputStream(item.getInputStream()).getBytes(),
									"UTF-8"));
						}else
						if (item.getFieldName().equals("descripcion")) {
							descripcion = Utilidades.escapeHtml(new String(getStringFromInputStream(item.getInputStream()).getBytes(),
									"UTF-8"));
						}else
						if (item.getFieldName().equals("link")) {
							link = Utilidades.escapeHtml(new String(getStringFromInputStream(item.getInputStream()).getBytes(),
									"UTF-8"));
						}else
						if (item.getFieldName().equals("posicion")) {
							posicion = Utilidades.escapeHtml(new String(getStringFromInputStream(item.getInputStream()).getBytes(),
									"UTF-8"));
						}

						if (!item.isFormField()) {
							DatosConexion datosCarpeta = new DatosConexion();
//							nombreImagen= nombre.replace(" ", "_").toLowerCase();
							nombreImagen=item.getName();
							if (item.getName() != null && !item.getName().equals("")) {
								item.write(new File(
										datosCarpeta.getPropiedad("carpeta.sliders") + "/" + item.getName()));
							}
						}
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}

			
			slider.setImagen(nombreImagen);
			slider.setPosicion(Integer.parseInt(posicion));
			if (!nombre.equals("")) {
				slider.setNombre(nombre);
			}
			if (!descripcion.equals("")) {
				slider.setDescripcion(descripcion);
			}
			if (!link.equals("")) {
				slider.setLink(link);
			}
			
			RegistroSlide registroSlide = new RegistroSlide();
			registroSlide.registrarSlide(slider);

			resp.sendRedirect("admin/listadoSliders.jsp");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

}
