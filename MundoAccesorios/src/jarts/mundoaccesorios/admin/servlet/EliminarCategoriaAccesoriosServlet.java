package jarts.mundoaccesorios.admin.servlet;

import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.admin.dao.EliminaAccesorioCategoria;
import jarts.mundoaccesorios.admin.dao.EliminaProducto;
import jarts.mundoaccesorios.entidades.Producto;
import jarts.mundoaccesorios.general.dao.ConsultaProducto;
import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;

public class EliminarCategoriaAccesoriosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	MultipartFormDataRequest mrequest = null;
	UploadBean upBean = null;
	UploadFile file;
	String ubicacionArchivo = "";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");

			Integer idCategoria = Integer.parseInt(req.getParameter("idCategoria"));
			ConsultaProducto consultaProducto = new ConsultaProducto();
			EliminaProducto eliminaProducto = new EliminaProducto();
			EliminaAccesorioCategoria eliminaAccesorioCategoria = new EliminaAccesorioCategoria();
			ArrayList<Producto> productos = consultaProducto.consultarProductosACCA(idCategoria);

			if (productos != null) {
				for (int i = 0; i < productos.size(); i++) {
					eliminaProducto.eliminarImagenProducto(productos.get(i).getId());
				}
			}

			eliminaProducto.eliminarProductosACCA(idCategoria);
			eliminaAccesorioCategoria.eliminarAccesorioCategoria(idCategoria);

			resp.sendRedirect("admin/categoriasAccesorios.jsp");

		} catch (

		Exception e)

		{
			e.printStackTrace();
		}
	}

}
