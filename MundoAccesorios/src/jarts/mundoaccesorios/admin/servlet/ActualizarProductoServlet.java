package jarts.mundoaccesorios.admin.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.admin.dao.ActualizaProducto;
import jarts.mundoaccesorios.entidades.Producto;
import jarts.mundoaccesorios.general.dao.Utilidades;

public class ActualizarProductoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");

			Integer productoId = Integer.parseInt(Utilidades.escapeHtml(req.getParameter("hidProducto")));
			String precio = Utilidades.escapeHtml(req.getParameter("precio"));
			String descripcion = Utilidades.escapeHtml(req.getParameter("descripcion"));
			String link = Utilidades.escapeHtml(req.getParameter("link"));
			String categoria = Utilidades.escapeHtml(req.getParameter("categoria"));
			String marca = Utilidades.escapeHtml(req.getParameter("marca"));
			String acca = Utilidades.escapeHtml(req.getParameter("acca"));
			
			Producto producto= new Producto();
			producto.setId(productoId);
			producto.setPrecio(Integer.parseInt(precio));
			producto.setDescripcion(descripcion);
			producto.setLink(link);
			producto.setIdCategoria(Integer.parseInt(categoria));
			producto.setIdMarca(Integer.parseInt(marca));
			producto.setIdAccesoriosCategoria(Integer.parseInt(acca));
			
			ActualizaProducto actualizaProducto = new ActualizaProducto();
			actualizaProducto.actualizarProducto(producto);
			
			resp.sendRedirect("listado.jsp?orden=1&pagina=1");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
