package jarts.mundoaccesorios.admin.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.entidades.Cliente;
import jarts.mundoaccesorios.general.dao.ConsultaCliente;
import jarts.mundoaccesorios.general.dao.Utilidades;

public class LogInServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");
			
			String email = Utilidades.escapeHtml(req.getParameter("email"));
			String password = Utilidades.escapeHtml(req.getParameter("password"));
			
			ConsultaCliente consultaCliente = new ConsultaCliente();
			Cliente cliente = consultaCliente.consultarCliente(email,password);
			
			if(cliente!=null){
				req.getSession().setAttribute("idCliente", cliente.getId());
				req.getSession().setAttribute("clienteRol", cliente.getRol());
				resp.sendRedirect("admin/menuAdministracion.jsp");
			}else{
				resp.sendRedirect("admin/login.jsp");
			}
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
