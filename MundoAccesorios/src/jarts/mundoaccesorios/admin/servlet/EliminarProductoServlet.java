package jarts.mundoaccesorios.admin.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.admin.dao.EliminaProducto;
import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;

public class EliminarProductoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	MultipartFormDataRequest mrequest = null;
	UploadBean upBean = null;
	UploadFile file;
	String ubicacionArchivo = "";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");
			
			Integer idProducto = Integer.parseInt(req.getParameter("idProducto"));
			EliminaProducto eliminaProducto = new EliminaProducto();
			eliminaProducto.eliminarImagenProducto(idProducto);
			eliminaProducto.eliminarProducto(idProducto);
			
			resp.sendRedirect("listado.jsp?orden=1&pagina=1");
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
