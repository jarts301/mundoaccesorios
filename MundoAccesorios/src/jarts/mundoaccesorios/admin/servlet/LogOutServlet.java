package jarts.mundoaccesorios.admin.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.entidades.Cliente;
import jarts.mundoaccesorios.general.dao.ConsultaCliente;
import jarts.mundoaccesorios.general.dao.Utilidades;

public class LogOutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");

			req.getSession().invalidate();

			resp.sendRedirect("index.jsp");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
