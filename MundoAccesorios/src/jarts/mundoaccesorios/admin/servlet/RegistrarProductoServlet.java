package jarts.mundoaccesorios.admin.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import jarts.mundoaccesorios.admin.dao.RegistroProducto;
import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Producto;
import jarts.mundoaccesorios.general.dao.Utilidades;
import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;

public class RegistrarProductoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	MultipartFormDataRequest mrequest = null;
	UploadBean upBean = null;
	UploadFile file;
	String ubicacionArchivo = "";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");
			
			Producto producto = new Producto();

			String nombre = "";
			String precio = "";
			String descripcion="";
			String link="";
			String categoria="";
			String marca="";
			String acca="";
			
			String nombreImagen= "";
			String extension = ".jpg";

			if (ServletFileUpload.isMultipartContent(req)) {
				try {
					List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(req);

					for (FileItem item : multiparts) {

						if (item.getFieldName().equals("nombre")) {
							nombre = Utilidades.escapeHtml(new String(getStringFromInputStream(item.getInputStream()).getBytes(),
									"UTF-8"));
						}else
						if (item.getFieldName().equals("precio")) {
							precio = Utilidades.escapeHtml(new String(getStringFromInputStream(item.getInputStream()).getBytes(),
									"UTF-8"));
						}else
						if (item.getFieldName().equals("descripcion")) {
							descripcion = Utilidades.escapeHtml(new String(getStringFromInputStream(item.getInputStream()).getBytes(),
									"UTF-8"));
						}else
						if (item.getFieldName().equals("link")) {
							link = Utilidades.escapeHtml(new String(getStringFromInputStream(item.getInputStream()).getBytes(),
									"UTF-8"));
						}else
						if (item.getFieldName().equals("categoria")) {
							categoria = Utilidades.escapeHtml(new String(getStringFromInputStream(item.getInputStream()).getBytes(),
									"UTF-8"));
						}else
						if (item.getFieldName().equals("marca")) {
							marca = Utilidades.escapeHtml(new String(getStringFromInputStream(item.getInputStream()).getBytes(),
									"UTF-8"));
						}else
						if (item.getFieldName().equals("acca")) {
							acca = Utilidades.escapeHtml(new String(getStringFromInputStream(item.getInputStream()).getBytes(),
									"UTF-8"));
						}

						if (!item.isFormField()) {
							DatosConexion datosCarpeta = new DatosConexion();
							nombreImagen= nombre.replace(" ", "_").toLowerCase();
							if (item.getName() != null && !item.getName().equals("")) {
								item.write(new File(
										datosCarpeta.getPropiedad("carpeta.imagenes") + "/" + nombreImagen + extension));
							}
						}
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}

			
			producto.setNombre(nombre);
			producto.setPrecio(Integer.parseInt(precio));
			producto.setImagen(nombreImagen+extension);
			producto.setFechaRegistro(new Date());
			producto.setIdCategoria(Integer.parseInt(categoria));
			producto.setIdMarca(Integer.parseInt(marca));
			if (!descripcion.equals("")) {
				producto.setDescripcion(descripcion);
			}
			if (!link.equals("")) {
				producto.setLink(link);
			}
			if (!acca.equals("")) {
				producto.setIdAccesoriosCategoria(Integer.parseInt(acca));
			}
			
			RegistroProducto registroProducto = new RegistroProducto();
			registroProducto.registrarProducto(producto);

			resp.sendRedirect("listado.jsp?orden=1&pagina=1");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

}
