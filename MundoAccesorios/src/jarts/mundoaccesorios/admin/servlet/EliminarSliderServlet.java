package jarts.mundoaccesorios.admin.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.admin.dao.EliminaProducto;
import jarts.mundoaccesorios.admin.dao.EliminaSlide;
import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;

public class EliminarSliderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	MultipartFormDataRequest mrequest = null;
	UploadBean upBean = null;
	UploadFile file;
	String ubicacionArchivo = "";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");
			
			Integer idSlider = Integer.parseInt(req.getParameter("idSlider"));
			EliminaSlide eliminaSlide = new EliminaSlide();
			eliminaSlide.eliminarImagenSlider(idSlider);
			eliminaSlide.eliminarSlide(idSlider);
			
			resp.sendRedirect("admin/listadoSliders.jsp");
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
