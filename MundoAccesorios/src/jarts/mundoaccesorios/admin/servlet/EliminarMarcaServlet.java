package jarts.mundoaccesorios.admin.servlet;

import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.admin.dao.EliminaAccesorioCategoria;
import jarts.mundoaccesorios.admin.dao.EliminaMarca;
import jarts.mundoaccesorios.admin.dao.EliminaProducto;
import jarts.mundoaccesorios.entidades.Producto;
import jarts.mundoaccesorios.general.dao.ConsultaProducto;
import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;

public class EliminarMarcaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	MultipartFormDataRequest mrequest = null;
	UploadBean upBean = null;
	UploadFile file;
	String ubicacionArchivo = "";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");

			Integer idMarca = Integer.parseInt(req.getParameter("idMarca"));
			ConsultaProducto consultaProducto = new ConsultaProducto();
			EliminaProducto eliminaProducto = new EliminaProducto();
			EliminaMarca eliminaMarca = new EliminaMarca();
			ArrayList<Producto> productos = consultaProducto.consultarProductosMarca(idMarca);

			if (productos != null) {
				for (int i = 0; i < productos.size(); i++) {
					eliminaProducto.eliminarImagenProducto(productos.get(i).getId());
				}
			}

			eliminaProducto.eliminarProductosMarca(idMarca);
			eliminaMarca.eliminarMarca(idMarca);

			resp.sendRedirect("admin/marcas.jsp");

		} catch (

		Exception e)

		{
			e.printStackTrace();
		}
	}

}
