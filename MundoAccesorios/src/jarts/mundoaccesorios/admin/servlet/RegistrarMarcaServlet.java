package jarts.mundoaccesorios.admin.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import jarts.mundoaccesorios.admin.dao.RegistroAccesorioCategoria;
import jarts.mundoaccesorios.admin.dao.RegistroMarca;
import jarts.mundoaccesorios.admin.dao.RegistroProducto;
import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.AccesoriosCategoria;
import jarts.mundoaccesorios.entidades.Marca;
import jarts.mundoaccesorios.entidades.Producto;
import jarts.mundoaccesorios.general.dao.Utilidades;
import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;

public class RegistrarMarcaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	MultipartFormDataRequest mrequest = null;
	UploadBean upBean = null;
	UploadFile file;
	String ubicacionArchivo = "";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");

			String nombre = req.getParameter("nombre");

			RegistroMarca registroMarca = new RegistroMarca();
			Marca marca = new Marca();
			marca.setNombre(nombre);
			registroMarca.registrarMarca(marca);

			resp.sendRedirect("admin/marcas.jsp");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
