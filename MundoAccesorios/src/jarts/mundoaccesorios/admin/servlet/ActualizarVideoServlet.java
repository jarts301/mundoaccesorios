package jarts.mundoaccesorios.admin.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.admin.dao.ActualizaProducto;
import jarts.mundoaccesorios.admin.dao.ActualizaVideo;
import jarts.mundoaccesorios.entidades.Producto;
import jarts.mundoaccesorios.general.dao.Utilidades;

public class ActualizarVideoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");

			String codigo = Utilidades.escapeHtml(req.getParameter("codigo"));
			
			ActualizaVideo actualizaVideo = new ActualizaVideo();
			actualizaVideo.actualizarVideo(codigo);
			
			resp.sendRedirect("admin/video.jsp");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
