package jarts.mundoaccesorios.entidades;

import java.util.Date;

public class Producto {
	
	private Integer Id;
	private String Nombre;
	private Integer Precio;
	private String Descripcion;
	private String Imagen;
	private String Link;
	private Boolean Destacado;
	private String Codigo;
	private Date FechaRegistro;
	private Integer IdCategoria;
	private Integer IdMarca;
	private Integer IdAccesoriosCategoria;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public Integer getPrecio() {
		return Precio;
	}
	public void setPrecio(Integer precio) {
		Precio = precio;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public String getImagen() {
		return Imagen;
	}
	public void setImagen(String imagen) {
		Imagen = imagen;
	}
	public String getLink() {
		return Link;
	}
	public void setLink(String link) {
		Link = link;
	}
	public Boolean getDestacado() {
		return Destacado;
	}
	public void setDestacado(Boolean destacado) {
		Destacado = destacado;
	}
	public String getCodigo() {
		return Codigo;
	}
	public void setCodigo(String codigo) {
		Codigo = codigo;
	}
	public Date getFechaRegistro() {
		return FechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		FechaRegistro = fechaRegistro;
	}
	public Integer getIdCategoria() {
		return IdCategoria;
	}
	public void setIdCategoria(Integer idCategoria) {
		IdCategoria = idCategoria;
	}
	public Integer getIdMarca() {
		return IdMarca;
	}
	public void setIdMarca(Integer idMarca) {
		IdMarca = idMarca;
	}
	public Integer getIdAccesoriosCategoria() {
		return IdAccesoriosCategoria;
	}
	public void setIdAccesoriosCategoria(Integer idAccesoriosCategoria) {
		IdAccesoriosCategoria = idAccesoriosCategoria;
	}

}
