package jarts.mundoaccesorios.entidades;

public class Slider {
	
	private Integer Id;
	private String Nombre;
	private String Descripcion;
	private String Imagen;
	private String Link;
	private Integer Posicion;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public String getImagen() {
		return Imagen;
	}
	public void setImagen(String imagen) {
		Imagen = imagen;
	}
	public String getLink() {
		return Link;
	}
	public void setLink(String link) {
		Link = link;
	}
	public Integer getPosicion() {
		return Posicion;
	}
	public void setPosicion(Integer posicion) {
		Posicion = posicion;
	}

}
