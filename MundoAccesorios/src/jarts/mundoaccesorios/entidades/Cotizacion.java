package jarts.mundoaccesorios.entidades;

public class Cotizacion {
	
	private Integer Id;
	private Integer IdProducto;
	private Integer IdCliente;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public Integer getIdProducto() {
		return IdProducto;
	}
	public void setIdProducto(Integer idProducto) {
		IdProducto = idProducto;
	}
	public Integer getIdCliente() {
		return IdCliente;
	}
	public void setIdCliente(Integer idCliente) {
		IdCliente = idCliente;
	}

}
