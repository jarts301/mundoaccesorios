package jarts.mundoaccesorios.entidades;

public class AccesoriosCategoria {
	
	private Integer Id;
	private String Nombre;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		this.Nombre = nombre;
	}

}
