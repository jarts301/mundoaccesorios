package jarts.mundoaccesorios.conexion;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DatosConexion {

	private Properties prop;
	private InputStream inputStream;

	public DatosConexion() {

		try {
			prop = new Properties();
			String propFileName = "/configuracion.properties";

			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public String getPropiedad(String nombre) {
		String propiedad;
		try {
			propiedad = prop.getProperty(nombre);
		} catch (Exception e) {
			propiedad = null;
		}
		return propiedad;
	}

}
