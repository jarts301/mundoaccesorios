package jarts.mundoaccesorios.general.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jarts.mundoaccesorios.general.dao.Utilidades;

public class CotizarProductoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");
			
			String idProducto = Utilidades.escapeHtml(req.getParameter("idProducto"));
			String cotizacion="";
			
			if(req.getSession().getAttribute("cotizacion")!=null){
				cotizacion = (String)req.getSession().getAttribute("cotizacion");
			}
			req.getSession().setAttribute("cotizacion", cotizacion+idProducto+",");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
