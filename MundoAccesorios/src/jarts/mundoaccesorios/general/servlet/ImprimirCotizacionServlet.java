package jarts.mundoaccesorios.general.servlet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Producto;
import jarts.mundoaccesorios.general.dao.ConsultaProducto;
import jarts.mundoaccesorios.general.dao.GeneradorCotizacionPDF;
import jarts.mundoaccesorios.general.dao.Utilidades;

public class ImprimirCotizacionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String cadenaFecha = (new Date()).toString().replace(" ", "").replace(":", "");

		req.setCharacterEncoding("UTF-8");

		DatosConexion propiedades = new DatosConexion();
		String rutaTemporales = propiedades.getPropiedad("carpeta.general");

		ConsultaProducto consultaProducto = new ConsultaProducto();
		String cotizacion = (String) req.getSession().getAttribute("cotizacion");
		HashMap<Integer, Integer> hmProductos = null;
		ArrayList<Producto> productosCotizacion = null;
		Integer cantidadProductosCot = 0;

		cantidadProductosCot = cotizacion.substring(0, cotizacion.length() - 1).split(",").length;
		hmProductos = consultaProducto.hacerMapCotizacion(cotizacion.substring(0, cotizacion.length() - 1).split(","));
		productosCotizacion = consultaProducto.consultarProductos(hmProductos);

		String rutaArchivo = rutaTemporales + "/cotizacion" + cadenaFecha + ".pdf";
		GeneradorCotizacionPDF genPDF = new GeneradorCotizacionPDF();
		genPDF.generarCotizacionPDF(rutaArchivo,hmProductos,productosCotizacion,cantidadProductosCot);
		generarDescarga(resp, rutaArchivo, "Cotizacion Mundo Accesorios.pdf", "pdf");

	}

	public void generarDescarga(HttpServletResponse response, String rutaArchivo, String nombreArchivo, String tipo) {
		try {

			response.setContentType("application/" + tipo);
			response.setHeader("Cache-Control", "no-cache"); // HTTP 1.1
			response.setHeader("Cache-Control", "max-age=0");
			response.setHeader("Content-disposition", "attachment; filename=" + nombreArchivo);
			ServletOutputStream stream = response.getOutputStream();
			FileInputStream input = new FileInputStream(rutaArchivo);
			BufferedInputStream buf = new BufferedInputStream(input);
			int readBytes = 0;

			while ((readBytes = buf.read()) != -1) {
				stream.write(readBytes);
			}
			stream.flush();
			buf.close();
			stream.close();

			File archivo = new File(rutaArchivo);
			archivo.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
