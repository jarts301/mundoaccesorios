package jarts.mundoaccesorios.general.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Slider;
import jarts.mundoaccesorios.general.dao.ConsultaSlider;
import jarts.mundoaccesorios.general.dao.Utilidades;

public class CargarImagenSliderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");
			
			DatosConexion datosCarpeta = new DatosConexion();

			String imagen = Utilidades.escapeHtml(req.getParameter("imagen"));
//			String extension = "";
//
//			ConsultaSlider consultaSlider = new ConsultaSlider();
//			Slider slider = consultaSlider.consultarSlider(Integer.parseInt(idSlider));
//
//			if (slider.getImagen().toLowerCase().endsWith(".jpg") || slider.getImagen().toLowerCase().endsWith(".jpeg")) {
//				resp.setContentType("image/jpeg");
//				extension = ".jpg";
//			}
//			if (slider.getImagen().toLowerCase().endsWith(".png")) {
//				resp.setContentType("image/png");
//				extension = ".png";
//			}
//			if (slider.getImagen().toLowerCase().endsWith(".bmp")) {
//				resp.setContentType("image/bmp");
//				extension = ".bmp";
//			}
//			if (slider.getImagen().toLowerCase().endsWith(".gif")) {
//				resp.setContentType("image/gif");
//				extension = ".gif";
//			}

			ServletOutputStream out;
			out = resp.getOutputStream();
			FileInputStream fin = new FileInputStream(
					datosCarpeta.getPropiedad("carpeta.sliders") + "/" + imagen);

			BufferedInputStream bin = new BufferedInputStream(fin);
			BufferedOutputStream bout = new BufferedOutputStream(out);
			int ch = 0;
			while ((ch = bin.read()) != -1) {
				bout.write(ch);
			}

			bin.close();
			fin.close();
			bout.close();
			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
