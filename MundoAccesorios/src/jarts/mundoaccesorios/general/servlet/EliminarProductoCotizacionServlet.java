package jarts.mundoaccesorios.general.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.general.dao.Utilidades;

public class EliminarProductoCotizacionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String idProducto = Utilidades.escapeHtml(request.getParameter("idProducto"));
		String[] cotizacion = ((String) request.getSession().getAttribute("cotizacion"))
				.substring(0, ((String) request.getSession().getAttribute("cotizacion")).length() - 1).split(",");

		String nuevaCotizacion = "";
		for (int i = 0; i < cotizacion.length; i++) {
			if (Integer.parseInt(cotizacion[i]) != Integer.parseInt(idProducto)) {
				nuevaCotizacion = nuevaCotizacion + cotizacion[i] + ",";
			}
		}
		
		if (!nuevaCotizacion.equals("")) {
			request.getSession().setAttribute("cotizacion", nuevaCotizacion);
		}else{
			request.getSession().removeAttribute("cotizacion");
		}

		response.sendRedirect("cart.jsp");

	}

}
