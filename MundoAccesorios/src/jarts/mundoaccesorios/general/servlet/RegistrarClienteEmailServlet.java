package jarts.mundoaccesorios.general.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarts.mundoaccesorios.general.dao.RegistroCliente;
import jarts.mundoaccesorios.general.dao.Utilidades;

public class RegistrarClienteEmailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.setCharacterEncoding("UTF-8");

			String email = Utilidades.escapeHtml(req.getParameter("Email"));
			
			RegistroCliente registroCliente = new RegistroCliente();
			registroCliente.registrarEmailCliente(email);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
