package jarts.mundoaccesorios.general.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Categoria;

public class ConsultaCategoria {
	
	private Connection conexion;
	private DatosConexion datosConexion;
	
	public Categoria consultarCategoria(Integer idCategoria) {
		
		String query = "select * from categoria where categoria_id="+idCategoria;

		Categoria categoria = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			while (resultado.next()) {
				categoria= new Categoria();
				categoria.setId(resultado.getInt("categoria_id"));
				categoria.setNombre(resultado.getString("categoria_nombre"));
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			categoria=null;
			e.printStackTrace();
		}
		
		return categoria;
	}
	
	public ArrayList<Categoria> consultarCategorias() {
		
		String query = "select categoria_id, categoria_nombre from categoria order by categoria_nombre";

		ArrayList<Categoria> categorias=null;
		Categoria categoria = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			categorias = new ArrayList<Categoria>();
			while (resultado.next()) {
				categoria= new Categoria();
				categoria.setId(resultado.getInt("categoria_id"));
				categoria.setNombre(resultado.getString("categoria_nombre"));
				categorias.add(categoria);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			categorias=null;
			e.printStackTrace();
		}
		if(categorias.size()<=0){
			categorias=null;
		}
		
		return categorias;
	}
}
