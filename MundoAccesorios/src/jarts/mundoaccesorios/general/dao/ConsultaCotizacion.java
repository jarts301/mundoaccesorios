package jarts.mundoaccesorios.general.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Cotizacion;

public class ConsultaCotizacion {
	
	private Connection conexion;
	private DatosConexion datosConexion;
	
	public Cotizacion consultarCotizacion(Integer idCotizacion) {
		
		String query = "select * from cotizacion where cotizacion_id="+idCotizacion;

		Cotizacion cotizacion = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			while (resultado.next()) {
				cotizacion= new Cotizacion();
				cotizacion.setId(resultado.getInt("producto_id"));
				cotizacion.setIdCliente(resultado.getInt("cliente_id"));
				cotizacion.setIdProducto(resultado.getInt("producto_id"));
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			cotizacion=null;
			e.printStackTrace();
		}
		
		return cotizacion;
	}
	
	public ArrayList<Cotizacion> consultarCotizaciones() {
		
		String query = "select * from cotizacion";

		ArrayList<Cotizacion> cotizaciones=null;
		Cotizacion cotizacion = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			cotizaciones = new ArrayList<Cotizacion>();
			while (resultado.next()) {
				cotizacion= new Cotizacion();
				cotizacion.setId(resultado.getInt("producto_id"));
				cotizacion.setIdCliente(resultado.getInt("cliente_id"));
				cotizacion.setIdProducto(resultado.getInt("producto_id"));
				cotizaciones.add(cotizacion);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			cotizaciones=null;
			e.printStackTrace();
		}
		if(cotizaciones.size()<=0){
			cotizaciones=null;
		}
		
		return cotizaciones;
	}
}
