package jarts.mundoaccesorios.general.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Slider;

public class ConsultaSlider {
	
	private Connection conexion;
	private DatosConexion datosConexion;
	
	public Slider consultarSlider(Integer idSlider) {
		
		String query = "select * from slider where slider_id="+idSlider;

		Slider slider = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			while (resultado.next()) {
				slider= new Slider();
				slider.setId(resultado.getInt("slider_id"));
				slider.setNombre(resultado.getString("slider_nombre"));
				slider.setDescripcion(resultado.getString("slider_descripcion"));
				slider.setImagen(resultado.getString("slider_imagen"));
				slider.setLink(resultado.getString("slider_link"));
				slider.setPosicion(resultado.getInt("slider_posicion"));
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			slider=null;
			e.printStackTrace();
		}
		
		return slider;
	}
	
	public ArrayList<Slider> consultarSliders() {
		
		String query = "select slider_id,slider_nombre,slider_descripcion,"
				+ "slider_imagen, slider_link, slider_posicion from slider order by slider_posicion";

		ArrayList<Slider> sliders=null;
		Slider slider = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			sliders = new ArrayList<Slider>();
			while (resultado.next()) {
				slider= new Slider();
				slider.setId(resultado.getInt("slider_id"));
				slider.setNombre(resultado.getString("slider_nombre"));
				slider.setDescripcion(resultado.getString("slider_descripcion"));
				slider.setImagen(resultado.getString("slider_imagen"));
				slider.setLink(resultado.getString("slider_link"));
				slider.setPosicion(resultado.getInt("slider_posicion"));
				sliders.add(slider);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			sliders=null;
			e.printStackTrace();
		}
		if(sliders.size()<=0){
			sliders=null;
		}
		
		return sliders;
	}
}
