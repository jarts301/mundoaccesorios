package jarts.mundoaccesorios.general.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Cliente;

public class ConsultaCliente {

	private Connection conexion;
	private DatosConexion datosConexion;

	public Cliente consultarCliente(Integer idCliente) {

		String query = "select * from cliente where cliente_id=" + idCliente;

		Cliente cliente = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			while (resultado.next()) {
				cliente = new Cliente();
				cliente.setId(resultado.getInt("cliente_id"));
				cliente.setEmail(resultado.getString("cliente_email"));
				cliente.setNombre(resultado.getString("cliente_nombre"));
				cliente.setTelefono(resultado.getString("cliente_telefono"));
				cliente.setDireccion(resultado.getString("cliente_direccion"));
				cliente.setPassword(resultado.getString("cliente_password"));
				cliente.setRol(resultado.getString("cliente_rol"));
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			cliente = null;
			e.printStackTrace();
		}

		return cliente;
	}

	public Cliente consultarCliente(String email, String password) {

		String query = "select * from cliente where lower(cliente_email)='" + email.toLowerCase()
				+ "' and cliente_password='" + password + "' and cliente_rol='administrador'" ;

		Cliente cliente = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			while (resultado.next()) {
				cliente = new Cliente();
				cliente.setId(resultado.getInt("cliente_id"));
				cliente.setEmail(resultado.getString("cliente_email"));
				cliente.setNombre(resultado.getString("cliente_nombre"));
				cliente.setTelefono(resultado.getString("cliente_telefono"));
				cliente.setDireccion(resultado.getString("cliente_direccion"));
				cliente.setPassword(resultado.getString("cliente_password"));
				cliente.setRol(resultado.getString("cliente_rol"));
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			cliente = null;
			e.printStackTrace();
		}

		return cliente;
	}

	public ArrayList<Cliente> consultarClientes() {

		String query = "select * from cliente";

		ArrayList<Cliente> clientes = null;
		Cliente cliente = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			clientes = new ArrayList<Cliente>();
			while (resultado.next()) {
				cliente = new Cliente();
				cliente.setId(resultado.getInt("cliente_id"));
				cliente.setEmail(resultado.getString("cliente_email"));
				cliente.setNombre(resultado.getString("cliente_nombre"));
				cliente.setTelefono(resultado.getString("cliente_telefono"));
				cliente.setDireccion(resultado.getString("cliente_direccion"));
				cliente.setPassword(resultado.getString("cliente_password"));
				cliente.setRol(resultado.getString("cliente_rol"));
				clientes.add(cliente);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			clientes = null;
			e.printStackTrace();
		}
		if (clientes.size() <= 0) {
			clientes = null;
		}

		return clientes;
	}

	public String consultarVideo() {
		String query = "select cliente_direccion from cliente where cliente_id=1";

		String resultado = "";

		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultSet = consulta.executeQuery(query);

			while (resultSet.next()) {
				resultado = resultSet.getString(1);
			}

			resultSet.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultado;
	}
}
