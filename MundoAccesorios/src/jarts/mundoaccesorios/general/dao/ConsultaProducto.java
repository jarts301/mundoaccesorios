package jarts.mundoaccesorios.general.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Producto;

public class ConsultaProducto {

	private Connection conexion;
	private DatosConexion datosConexion;

	public Producto consultarProducto(Integer idProducto) {

		String query = "select * from producto where producto_id=" + idProducto;

		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setCodigo(resultado.getString("producto_codigo"));
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			producto = null;
			e.printStackTrace();
		}

		return producto;
	}

	public ArrayList<Producto> consultarProductos() {

		String query = "select * from producto";

		ArrayList<Producto> productos = null;
		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			productos = new ArrayList<Producto>();
			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setCodigo(resultado.getString("producto_codigo"));
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
				productos.add(producto);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			productos = null;
			e.printStackTrace();
		}
		if (productos.size() <= 0) {
			productos = null;
		}

		return productos;
	}

	public ArrayList<Producto> consultarProductosACCA(Integer idCategoria) {

		String query = "select * from producto where acca_id="+idCategoria;

		ArrayList<Producto> productos = null;
		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			productos = new ArrayList<Producto>();
			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setCodigo(resultado.getString("producto_codigo"));
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
				productos.add(producto);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			productos = null;
			e.printStackTrace();
		}
		if (productos.size() <= 0) {
			productos = null;
		}

		return productos;
	}
	
	public ArrayList<Producto> consultarProductosMarca(Integer idMarca) {

		String query = "select * from producto where marca_id="+idMarca;

		ArrayList<Producto> productos = null;
		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			productos = new ArrayList<Producto>();
			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setCodigo(resultado.getString("producto_codigo"));
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
				productos.add(producto);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			productos = null;
			e.printStackTrace();
		}
		if (productos.size() <= 0) {
			productos = null;
		}

		return productos;
	}
	
	public ArrayList<Producto> consultarProductos(HashMap<Integer, Integer> hmProductos) {
		String query = "select * from producto where ";

		Iterator<Integer> keySetIterator = hmProductos.keySet().iterator();
		while (keySetIterator.hasNext()) {
			Integer key = keySetIterator.next();
			query = query + "producto_id=" + key + " or ";
		}
		query = query.substring(0, query.length() - 4);

		ArrayList<Producto> productos = null;
		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			productos = new ArrayList<Producto>();
			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setCodigo(resultado.getString("producto_codigo"));
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
				productos.add(producto);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			productos = null;
			e.printStackTrace();
		}
		if (productos.size() <= 0) {
			productos = null;
		}

		return productos;
	}

	public ArrayList<Producto> consultarProductos(Integer categoria, Integer marca, Integer pagina, Integer orden) {
		String textoOrden = "order by " + definirOrden(orden);

		String query = "select producto_id, producto_nombre, producto_precio, producto_descripcion, "
				+ "producto_destacado, producto_imagen,producto_link, producto_fecha, categoria_id, marca_id, acca_id "
				+ "from producto where categoria_id=" + categoria + " and marca_id=" + marca + " " + textoOrden
				+ " limit " + (pagina - 1) * 8 + "," + 8;

		ArrayList<Producto> productos = null;
		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			productos = new ArrayList<Producto>();
			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
				productos.add(producto);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			productos = null;
			e.printStackTrace();
		}
		if (productos.size() <= 0) {
			productos = null;
		}

		return productos;
	}

	public ArrayList<Producto> consultarProductos(Integer categoriaAccesorios, Integer pagina, Integer orden) {
		String textoOrden = "order by " + definirOrden(orden);

		String query = "select producto_id, producto_nombre, producto_precio, producto_descripcion, "
				+ "producto_destacado, producto_imagen,producto_link, producto_fecha, categoria_id, marca_id, acca_id "
				+ "from producto where acca_id=" + categoriaAccesorios + " " + textoOrden + " limit " + (pagina - 1) * 8
				+ "," + 8;

		ArrayList<Producto> productos = null;
		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			productos = new ArrayList<Producto>();
			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
				productos.add(producto);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			productos = null;
			e.printStackTrace();
		}
		if (productos.size() <= 0) {
			productos = null;
		}

		return productos;
	}

	public ArrayList<Producto> consultarProductos(String terminoBusqueda, Integer pagina, Integer orden) {
		String textoOrden = "order by " + definirOrden(orden);

		String query = "select producto_id, producto_nombre, producto_precio, producto_descripcion, "
				+ "producto_destacado, producto_imagen,producto_link, producto_fecha, categoria_id, marca_id, acca_id "
				+ "from producto where " + "lower(producto_nombre) like '%" + terminoBusqueda.toLowerCase() + "%' or "
				+ "categoria_id in (select categoria_id from categoria where lower(categoria_nombre) like '%"
				+ terminoBusqueda.toLowerCase() + "%') or "
				+ "marca_id in (select marca_id from marca where lower(marca_nombre) like '%"
				+ terminoBusqueda.toLowerCase() + "%') or "
				+ "acca_id in (select acca_id from accesorios_categoria where lower(acca_nombre) like '%"
				+ terminoBusqueda.toLowerCase() + "%') " + textoOrden + " limit " + (pagina - 1) * 8 + "," + 8;

		ArrayList<Producto> productos = null;
		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			productos = new ArrayList<Producto>();
			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
				productos.add(producto);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			productos = null;
			e.printStackTrace();
		}
		if (productos.size() <= 0) {
			productos = null;
		}

		return productos;
	}

	public ArrayList<Producto> consultarProductos(Integer pagina, Integer orden) {
		String textoOrden = "order by " + definirOrden(orden);

		String query = "select producto_id, producto_nombre, producto_precio, producto_descripcion, "
				+ "producto_destacado, producto_imagen,producto_link, producto_fecha, categoria_id, marca_id, acca_id "
				+ "from producto " + textoOrden + " limit " + (pagina - 1) * 8 + "," + 8;

		ArrayList<Producto> productos = null;
		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			productos = new ArrayList<Producto>();
			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
				productos.add(producto);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			productos = null;
			e.printStackTrace();
		}
		if (productos.size() <= 0) {
			productos = null;
		}

		return productos;
	}

	public ArrayList<Producto> consultarProductosDestacados() {

		String query = "select * from producto where producto_destacado=1 limit 6";

		ArrayList<Producto> productos = null;
		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			productos = new ArrayList<Producto>();
			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setCodigo(resultado.getString("producto_codigo"));
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
				productos.add(producto);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			productos = null;
			e.printStackTrace();
		}
		if (productos.size() <= 0) {
			productos = null;
		}

		return productos;
	}

	public ArrayList<Producto> consultarCelularesYTabletsAzar() {

		String query = "SELECT * FROM producto where categoria_id =1 or categoria_id =2 ORDER BY RAND() LIMIT 8";

		ArrayList<Producto> productos = null;
		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			productos = new ArrayList<Producto>();
			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setCodigo(resultado.getString("producto_codigo"));
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
				productos.add(producto);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			productos = null;
			e.printStackTrace();
		}
		if (productos.size() <= 0) {
			productos = null;
		}

		return productos;
	}

	public ArrayList<Producto> consultarAccesoriosAzar() {

		String query = "SELECT * FROM producto where categoria_id =5 ORDER BY RAND() LIMIT 8";

		ArrayList<Producto> productos = null;
		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			productos = new ArrayList<Producto>();
			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setCodigo(resultado.getString("producto_codigo"));
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
				productos.add(producto);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			productos = null;
			e.printStackTrace();
		}
		if (productos.size() <= 0) {
			productos = null;
		}

		return productos;
	}

	public ArrayList<Producto> consultarPortatilesYComputadoresAzar() {

		String query = "SELECT * FROM producto where categoria_id =3 or categoria_id =4 ORDER BY RAND() LIMIT 8";

		ArrayList<Producto> productos = null;
		Producto producto = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);

			productos = new ArrayList<Producto>();
			while (resultado.next()) {
				producto = new Producto();
				producto.setId(resultado.getInt("producto_id"));
				producto.setNombre(resultado.getString("producto_nombre"));
				producto.setPrecio(resultado.getInt("producto_precio"));
				producto.setDescripcion(resultado.getString("producto_descripcion"));
				producto.setImagen(resultado.getString("producto_imagen"));
				producto.setLink(resultado.getString("producto_link"));
				producto.setDestacado(resultado.getInt("producto_destacado") != 0);
				producto.setCodigo(resultado.getString("producto_codigo"));
				producto.setFechaRegistro(resultado.getTimestamp("producto_fecha"));
				producto.setIdCategoria(resultado.getInt("categoria_id"));
				producto.setIdMarca(resultado.getInt("marca_id"));
				producto.setIdAccesoriosCategoria(resultado.getInt("acca_id"));
				productos.add(producto);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			productos = null;
			e.printStackTrace();
		}
		if (productos.size() <= 0) {
			productos = null;
		}

		return productos;
	}

	public Integer consultarCantidadProductos(Integer categoria, Integer marca) {
		Integer resultado = 0;

		String query = "select count(*) from producto where categoria_id=" + categoria + " and marca_id=" + marca;

		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultSet = consulta.executeQuery(query);

			while (resultSet.next()) {
				resultado = resultSet.getInt(1);
			}

			resultSet.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			resultado = 0;
			e.printStackTrace();
		}

		return resultado;
	}

	public Integer consultarCantidadProductos(Integer categoriaAccesorios) {
		Integer resultado = 0;

		String query = "select count(*) from producto where acca_id=" + categoriaAccesorios;

		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultSet = consulta.executeQuery(query);

			while (resultSet.next()) {
				resultado = resultSet.getInt(1);
			}

			resultSet.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			resultado = 0;
			e.printStackTrace();
		}

		return resultado;
	}

	public Integer consultarCantidadProductos(String terminoBusqueda) {
		Integer resultado = 0;

		String query = "select count(*) from producto where " + "lower(producto_nombre) like '%"
				+ terminoBusqueda.toLowerCase() + "%' or "
				+ "categoria_id in (select categoria_id from categoria where lower(categoria_nombre) like '%"
				+ terminoBusqueda.toLowerCase() + "%') or "
				+ "marca_id in (select marca_id from marca where lower(marca_nombre) like '%"
				+ terminoBusqueda.toLowerCase() + "%') or "
				+ "acca_id in (select acca_id from accesorios_categoria where lower(acca_nombre) like '%"
				+ terminoBusqueda.toLowerCase() + "%') ";

		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultSet = consulta.executeQuery(query);

			while (resultSet.next()) {
				resultado = resultSet.getInt(1);
			}

			resultSet.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			resultado = 0;
			e.printStackTrace();
		}

		return resultado;
	}

	public Integer consultarCantidadProductos() {
		Integer resultado = 0;

		String query = "select count(*) from producto";

		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultSet = consulta.executeQuery(query);

			while (resultSet.next()) {
				resultado = resultSet.getInt(1);
			}

			resultSet.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			resultado = 0;
			e.printStackTrace();
		}

		return resultado;
	}

	public String definirOrden(Integer idOrden) {
		String resultado = "";
		switch (idOrden) {
		case 1:// Mas reciente
			resultado += " producto_fecha desc";
			break;
		case 2:// Nombres
			resultado += " producto_nombre";
			break;

		case 3:// Menor precio
			resultado += " producto_precio";
			break;

		case 4:// Mayor precio
			resultado += " producto_precio desc";
			break;
		}
		return resultado;
	}

	public HashMap<Integer, Integer> hacerMapCotizacion(String[] idsProductos) {
		HashMap<Integer, Integer> resultado = new HashMap<Integer, Integer>();

		for (int i = 0; i < idsProductos.length; i++) {
			if (resultado.get(Integer.parseInt(idsProductos[i])) != null) {
				resultado.put(Integer.parseInt(idsProductos[i]), resultado.get(Integer.parseInt(idsProductos[i])) + 1);
			} else {
				resultado.put(Integer.parseInt(idsProductos[i]), 1);
			}
		}

		return resultado;
	}

	public Producto obtenerProducto(ArrayList<Producto> productos, Integer idProducto) {
		Producto producto = null;

		for (int i = 0; i < productos.size(); i++) {
			if (productos.get(i).getId() == idProducto) {
				producto = productos.get(i);
				break;
			}
		}

		return producto;
	}

}
