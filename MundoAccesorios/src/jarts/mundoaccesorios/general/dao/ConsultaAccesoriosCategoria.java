package jarts.mundoaccesorios.general.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.AccesoriosCategoria;

public class ConsultaAccesoriosCategoria {
	
	private Connection conexion;
	private DatosConexion datosConexion;
	
	public AccesoriosCategoria consultarAccesoriosCategoria(Integer idAccesoriosCategoria) {
		
		String query = "select * from accesorios_categoria where acca_id="+idAccesoriosCategoria;

		AccesoriosCategoria accesoriosCategoria = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			while (resultado.next()) {
				accesoriosCategoria= new AccesoriosCategoria();
				accesoriosCategoria.setId(resultado.getInt("acca_id"));
				accesoriosCategoria.setNombre(resultado.getString("acca_nombre"));
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			accesoriosCategoria=null;
			e.printStackTrace();
		}
		
		return accesoriosCategoria;
	}
	
	public ArrayList<AccesoriosCategoria> consultarAccesoriosCategorias() {
		
		String query = "select acca_id, acca_nombre from accesorios_categoria order by acca_nombre";

		ArrayList<AccesoriosCategoria> accesoriosCategorias=null;
		AccesoriosCategoria accesoriosCategoria = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			accesoriosCategorias = new ArrayList<AccesoriosCategoria>();
			while (resultado.next()) {
				accesoriosCategoria= new AccesoriosCategoria();
				accesoriosCategoria.setId(resultado.getInt("acca_id"));
				accesoriosCategoria.setNombre(resultado.getString("acca_nombre"));
				accesoriosCategorias.add(accesoriosCategoria);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			accesoriosCategorias=null;
			e.printStackTrace();
		}
		if(accesoriosCategorias.size()<=0){
			accesoriosCategorias=null;
		}
		
		return accesoriosCategorias;
	}
}
