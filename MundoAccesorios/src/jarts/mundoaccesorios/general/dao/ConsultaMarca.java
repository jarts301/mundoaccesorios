package jarts.mundoaccesorios.general.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Marca;

public class ConsultaMarca {
	
	private Connection conexion;
	private DatosConexion datosConexion;
	
	public Marca consultarMarca(Integer idMarca) {
		
		String query = "select * from marca where marca_id="+idMarca;

		Marca marca = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			while (resultado.next()) {
				marca= new Marca();
				marca.setId(resultado.getInt("marca_id"));
				marca.setNombre(resultado.getString("marca_nombre"));
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			marca=null;
			e.printStackTrace();
		}
		
		return marca;
	}
	
	public ArrayList<Marca> consultarMarcas() {
		
		String query = "select marca_id, marca_nombre from marca order by marca_nombre";

		ArrayList<Marca> marcas=null;
		Marca marca = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			marcas = new ArrayList<Marca>();
			while (resultado.next()) {
				marca= new Marca();
				marca.setId(resultado.getInt("marca_id"));
				marca.setNombre(resultado.getString("marca_nombre"));
				marcas.add(marca);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			marcas=null;
			e.printStackTrace();
		}
		if(marcas.size()<=0){
			marcas=null;
		}
		
		return marcas;
	}
	
	public ArrayList<Marca> consultarMarcasConCelulares() {
		
		String query = "select marca_id, marca_nombre from marca where marca_id in "
					+ "(select marca_id from producto where categoria_id=1) order by marca_nombre";

		ArrayList<Marca> marcas=null;
		Marca marca = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			marcas = new ArrayList<Marca>();
			while (resultado.next()) {
				marca= new Marca();
				marca.setId(resultado.getInt("marca_id"));
				marca.setNombre(resultado.getString("marca_nombre"));
				marcas.add(marca);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			marcas=null;
			e.printStackTrace();
		}
		if(marcas.size()<=0){
			marcas=null;
		}
		
		return marcas;
	}
	
	public ArrayList<Marca> consultarMarcasConTablets() {
		
		String query = "select marca_id, marca_nombre from marca where marca_id in "
					+ "(select marca_id from producto where categoria_id=2) order by marca_nombre";

		ArrayList<Marca> marcas=null;
		Marca marca = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			marcas = new ArrayList<Marca>();
			while (resultado.next()) {
				marca= new Marca();
				marca.setId(resultado.getInt("marca_id"));
				marca.setNombre(resultado.getString("marca_nombre"));
				marcas.add(marca);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			marcas=null;
			e.printStackTrace();
		}
		if(marcas.size()<=0){
			marcas=null;
		}
		
		return marcas;
	}
	
	public ArrayList<Marca> consultarMarcasConPortatiles() {
		
		String query = "select marca_id, marca_nombre from marca where marca_id in "
					+ "(select marca_id from producto where categoria_id=3) order by marca_nombre";

		ArrayList<Marca> marcas=null;
		Marca marca = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			marcas = new ArrayList<Marca>();
			while (resultado.next()) {
				marca= new Marca();
				marca.setId(resultado.getInt("marca_id"));
				marca.setNombre(resultado.getString("marca_nombre"));
				marcas.add(marca);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			marcas=null;
			e.printStackTrace();
		}
		if(marcas.size()<=0){
			marcas=null;
		}
		
		return marcas;
	}
	
	public ArrayList<Marca> consultarMarcasConComputadores() {
		
		String query = "select marca_id, marca_nombre from marca where marca_id in "
					+ "(select marca_id from producto where categoria_id=4) order by marca_nombre";

		ArrayList<Marca> marcas=null;
		Marca marca = null;
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			ResultSet resultado = consulta.executeQuery(query);
			
			marcas = new ArrayList<Marca>();
			while (resultado.next()) {
				marca= new Marca();
				marca.setId(resultado.getInt("marca_id"));
				marca.setNombre(resultado.getString("marca_nombre"));
				marcas.add(marca);
			}

			resultado.close();
			consulta.close();
			conexion.close();

		} catch (Exception e) {
			marcas=null;
			e.printStackTrace();
		}
		if(marcas.size()<=0){
			marcas=null;
		}
		
		return marcas;
	}
}
