package jarts.mundoaccesorios.general.dao;

public class Utilidades {
	
	public static String escapeHtml(String cadena){
		if(cadena == null){
			return null;
		} else {
			String resultado="";
			resultado= cadena
//					.replace("&", "&amp;")
					.replace(">", "&gt;")
					.replace("<", "&lt;");
//					.replace("'", "&#039;");
//					.replace("\"", "&quot;");
			
			return resultado;
		}
	}
	
	public static String escapeHtml2(String cadena){
		if(cadena == null){
			return null;
		} else {		
			String resultado="";
			
			resultado= cadena
					.replace(">", "&gt;")
					.replace("<", "&lt;");
//					.replace("&", "&amp;")
//					.replace("'", "&#039;")
//					.replace("\"", "&quot;");
			
			return resultado;
		}
	}

}
