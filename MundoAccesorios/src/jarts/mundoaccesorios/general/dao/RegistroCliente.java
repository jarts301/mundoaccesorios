package jarts.mundoaccesorios.general.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import jarts.mundoaccesorios.conexion.DatosConexion;

public class RegistroCliente {
	
	private Connection conexion;
	private DatosConexion datosConexion;

	public Integer registrarEmailCliente(String emailCliente) {
		Integer res=-1;
		
		String query = "insert into cliente(cliente_email,cliente_rol) " + "values('" + emailCliente+ "','usuario')";
		try {
			datosConexion = new DatosConexion();
			conexion = DriverManager.getConnection(datosConexion.getPropiedad("url"),
					datosConexion.getPropiedad("usuario"), datosConexion.getPropiedad("password"));
			Statement consulta = conexion.createStatement();
			consulta.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
			
			ResultSet resultSet = consulta.getGeneratedKeys();
			
			while (resultSet.next()) {
				res=resultSet.getInt(1);
			}

			consulta.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			res=-1;
		}
		return res;
	}

}
