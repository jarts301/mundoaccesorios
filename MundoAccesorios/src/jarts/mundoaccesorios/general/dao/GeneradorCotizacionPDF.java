package jarts.mundoaccesorios.general.dao;

import java.awt.Color;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.codec.TiffImage;

import jarts.mundoaccesorios.conexion.DatosConexion;
import jarts.mundoaccesorios.entidades.Producto;

public class GeneradorCotizacionPDF {

	private Font fuenteTitulo = new Font(Font.FontFamily.UNDEFINED, 17, Font.BOLD);
	private Font fuenteSubtitulo = new Font(Font.FontFamily.UNDEFINED, 15, Font.BOLD);
	private Font fuenteSubtitulo2 = new Font(Font.FontFamily.UNDEFINED, 13, Font.ITALIC);

	private HashMap<Integer, Integer> cantidadesProductos = new HashMap<Integer, Integer>();
	private ArrayList<Producto> productos = new ArrayList<Producto>();
	private Integer cantidadTotalProductos = 0;

	private DecimalFormat formatoNumeros = new DecimalFormat("###,###.##");

	/** genera un documento pdf con contenido relacionado a un actor */
	public void generarCotizacionPDF(String ruta, HashMap<Integer, Integer> hmProductos, ArrayList<Producto> productos,
			Integer numProductos) {

		this.cantidadesProductos = hmProductos;
		this.productos = productos;
		this.cantidadTotalProductos = numProductos;

		try {
			Document document = new Document(PageSize.A4, -20f, -20f, 20f, 20f);
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(ruta));
			document.open();
			addMetaDataPDF(document);
			//poneSelloAgua(writer);
			agregarDatosPerfilActor(document);
			//DatosConexion datosConexion = new DatosConexion();
			//poneImagen(document, datosConexion.getPropiedad("carpeta.general")+"logo.tiff");
			document.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** genera metadatos basicos al documento pdf */
	private void addMetaDataPDF(Document document) {
		document.addTitle("Cotizacion");
		document.addSubject("Cotizacion");
		document.addKeywords("mundo, accesorios, tecnologia");
		document.addAuthor("Mundo Accesorios");
		document.addCreator("Mundo Accesorios");
	}

	/** agrega a un documento pdf contenido relacionado a un actor */
	private void agregarDatosPerfilActor(Document document) throws DocumentException {
		try{
		Paragraph preface = null;
		
		float[] medCeldaInfo = { 2.0f , 2.75f };
		PdfPTable tablaTitulo = new PdfPTable(2);
		tablaTitulo.setWidths(medCeldaInfo);
//		DatosConexion datosConexion = new DatosConexion();
//		Image image = Image.getInstance(datosConexion.getPropiedad("carpeta.general")+"logo.jpg");
		PdfPCell celda = new PdfPCell(new Phrase("MUNDO ACCESORIOS", fuenteTitulo));
		celda.setHorizontalAlignment(Element.ALIGN_LEFT);
		celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
		celda.setBorder(0);
		tablaTitulo.addCell(celda);
		celda = new PdfPCell(new Phrase("Tel�fono:\n\t(+57) (5) 3177864\nDirecci�n:\n\tC.C Parque Central Local 190 y 198 'A'\n\tBarranquilla, Colombia", fuenteSubtitulo2));
		celda.setHorizontalAlignment(Element.ALIGN_LEFT);
		celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
		celda.setBorder(0);
		tablaTitulo.addCell(celda);
		document.add(tablaTitulo);
		
		preface = new Paragraph();
		preface.add(new Paragraph("\n"));
		document.add(preface);

		float[] medCeldaTitulo = { 5.75f };
		tablaTitulo = new PdfPTable(1);
		tablaTitulo.setWidths(medCeldaTitulo);
		celda = new PdfPCell(new Phrase("COTIZACI�N ("+cantidadTotalProductos+(cantidadTotalProductos>1?" PRODUCTOS )":" PRODUCTO )"), fuenteTitulo));
		celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		celda.setBorder(0);
		tablaTitulo.addCell(celda);
		document.add(tablaTitulo);

		preface = new Paragraph();
		preface.add(new Paragraph("\n"));
		document.add(preface);

		float[] datosBasicos = { 1.5f, 2.0f, 1.0f, 1.0f };
		PdfPTable tablaDatosBasicos = new PdfPTable(4);
		tablaDatosBasicos.setWidths(datosBasicos);

		celda = new PdfPCell(new Phrase("PRODUCTO", fuenteSubtitulo2));
		celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		tablaDatosBasicos.addCell(celda);
		celda = new PdfPCell(new Phrase("DESCRIPCI�N", fuenteSubtitulo2));
		celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		tablaDatosBasicos.addCell(celda);
		celda = new PdfPCell(new Phrase("CANTIDAD", fuenteSubtitulo2));
		celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		tablaDatosBasicos.addCell(celda);
		celda = new PdfPCell(new Phrase("PRECIO UNITARIO", fuenteSubtitulo2));
		celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		tablaDatosBasicos.addCell(celda);

		Producto productoActual = null;
		for (int i = 0; i < productos.size(); i++) {

			productoActual = productos.get(i);

			celda = new PdfPCell(new Phrase(productoActual.getNombre()));
			celda.setHorizontalAlignment(Element.ALIGN_CENTER);
			celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
			tablaDatosBasicos.addCell(celda);
			celda = new PdfPCell(new Phrase(productoActual.getDescripcion()));
			celda.setHorizontalAlignment(Element.ALIGN_CENTER);
			celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
			tablaDatosBasicos.addCell(celda);
			celda = new PdfPCell(new Phrase( cantidadesProductos.get(productoActual.getId()).toString() ));
			celda.setHorizontalAlignment(Element.ALIGN_CENTER);
			celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
			tablaDatosBasicos.addCell(celda);
			celda = new PdfPCell(new Phrase("$" + formatoNumeros.format(productoActual.getPrecio())));
			celda.setHorizontalAlignment(Element.ALIGN_CENTER);
			celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
			tablaDatosBasicos.addCell(celda);
		}

		document.add(tablaDatosBasicos);

		preface = new Paragraph();
		preface.add(new Paragraph("\n"));
		document.add(preface);
		
		float[] medCeldasTotal = { 4.0f, 1.7f };
		PdfPTable tablaTotal = new PdfPTable(2);
		tablaTotal.setWidths(medCeldasTotal);
		PdfPCell celdaTotal = new PdfPCell(new Phrase("TOTAL:", fuenteSubtitulo));
		celdaTotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
		celdaTotal.setBorder(0);
		tablaTotal.addCell(celdaTotal);
		celdaTotal = new PdfPCell(new Phrase( "$"+formatoNumeros.format(calcularPrecioTotal()), fuenteSubtitulo));
		celdaTotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
		celdaTotal.setBorder(0);
		tablaTotal.addCell(celdaTotal);
		document.add(tablaTotal);
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public Integer calcularPrecioTotal() {
		Integer resultado = 0;
		ConsultaProducto consultaProducto = new ConsultaProducto();
		if (cantidadesProductos != null) {
			Iterator<Integer> keySetIterator = cantidadesProductos.keySet().iterator();
			while (keySetIterator.hasNext()) {
				Integer key = keySetIterator.next();
				resultado = resultado
						+ (cantidadesProductos.get(key) * consultaProducto.obtenerProducto(productos, key).getPrecio());
			}
		}

		return resultado;
	}
	
	private void poneImagen(Document document, String archivo) {
        RandomAccessFileOrArray ra = null;
        try {
            ra = new RandomAccessFileOrArray(archivo);
            Image img = TiffImage.getTiffImage(ra, 1);
            if (img != null) {
                //Se valida la escala de la im�gen 
                if (img.getScaledWidth() > 600 || img.getScaledHeight() > 800) {
                    //Se reduce la escala de la im�gen     
                    img.scaleToFit(615, 825);
                }
                //Se indica la posicion donde se colocara la im�gen en el PDF
                img.setAbsolutePosition(0, 0);
                //Se inserta la im�gen en el PDF
                document.add(img);              
            }
            ra.close();
        }
        catch (Exception e) {
            System.out.println("Error al agregar imagen al PDF " + e.getMessage());
        }       
    }
	
    private static void poneSelloAgua(PdfWriter writer) {
        try {
            PdfContentByte cb = writer.getDirectContent();
            //Se crea un templete para asignar la marca de agua
            PdfTemplate template = cb.createTemplate(700, 300);
            template.beginText();
            //Inicializamos los valores para el templete
            //Se define el tipo de letra, color y tama�o
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            template.setColorFill(new BaseColor(220, 220, 220));
            template.setFontAndSize(bf, 40);
            
            template.setTextMatrix(0, 0);
            //Se define el texto que se agregara como marca de agua
            template.showText("MUNDO ACCESORIOS");
            template.endText();
            //Se asigna el templete
            //Se asignan los valores para el texto de marca de agua
            // Se asigna el grado de inclinacion y la posicion donde aparecer� el texto
            //                                                     x    y
            cb.addTemplate(template, 1, 1, -1,1, 100, 200);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
